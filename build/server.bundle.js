/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 28);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("classnames");

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("react-router-dom");

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var secretString = exports.secretString = 'gds_%*ghj654657if#$kj%m@cnb#r6hd#seg5%6esbsgr5^6tgdtb';
var accessKeyId = exports.accessKeyId = 'AKIAJQGCQU7APS4VZMHA';
var secretAccessKey = exports.secretAccessKey = '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';
var mailLogin = exports.mailLogin = 'cmsserver12@gmail.com';
var mailPassword = exports.mailPassword = 'cms1234-';

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createDummyPosts = createDummyPosts;
exports.resetPosts = resetPosts;
exports.resetPost = resetPost;
exports.getPosts = getPosts;
exports.addComment = addComment;
exports.getPost = getPost;
exports.getPostsCount = getPostsCount;

var _axios = __webpack_require__(9);

var _axios2 = _interopRequireDefault(_axios);

var _postActionTypes = __webpack_require__(24);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createDummyPosts(count) {
  return function (dispatch) {
    dispatch({
      type: _postActionTypes.GET_DUMMY_POSTS,
      payload: {
        length: count
      }
    });
  };
}
function resetPosts() {
  return function (dispatch) {
    dispatch({
      type: _postActionTypes.RESET_POSTS
    });
  };
}
function resetPost() {
  return function (dispatch) {
    dispatch({
      type: _postActionTypes.RESET_POST
    });
  };
}

function getPosts(skip) {
  return function (dispatch) {
    _axios2.default.get('/api/post/get/' + skip + '/9').then(function (response) {
      dispatch({
        type: _postActionTypes.REPLACE_DUMMY_POSTS,
        payload: {
          posts: response.data
        }
      });
    });
  };
}
function addComment(data) {
  return function (dispatch) {
    _axios2.default.post('api/post/add-comment', data).then(function (response) {
      dispatch({
        type: _postActionTypes.ADD_COMMENT,
        payload: {
          comment: response.data
        }
      });
    });
  };
}

function getPost(_id) {
  return function (dispatch) {
    _axios2.default.post('/api/post/get', { _id: _id }).then(function (response) {
      dispatch({
        type: _postActionTypes.GET_MAIN_POST,
        payload: {
          post: response.data
        }
      });
    });
  };
}

function getPostsCount() {
  return function (dispatch) {
    _axios2.default.post('/api/post/count', {}).then(function (response) {
      dispatch({
        type: _postActionTypes.GET_POST_COUNT,
        payload: {
          count: response.data.count
        }
      });
    });
  };
}

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactAddonsCssTransitionGroup = __webpack_require__(45);

var _reactAddonsCssTransitionGroup2 = _interopRequireDefault(_reactAddonsCssTransitionGroup);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fadeTransition = function fadeTransition(WrappedComponent) {
  var TransitionedComponent = function TransitionedComponent(props) {
    return _react2.default.createElement(
      _reactAddonsCssTransitionGroup2.default,
      {
        transitionAppear: true,
        transitionLeave: true,
        transitionAppearTimeout: 600,
        transitionEnterTimeout: 600,
        transitionLeaveTimeout: 400,
        transitionName: 'fade'
      },
      _react2.default.createElement(WrappedComponent, props)
    );
  };
  return TransitionedComponent;
};

exports.default = fadeTransition;

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("react-helmet");

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("babel-polyfill");

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("react-router-config");

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = require("morgan");

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.signOutUser = exports.getUser = exports.resetErrors = exports.signOut = exports.signIn = exports.signUp = undefined;
exports.toggleModal = toggleModal;

var _axios = __webpack_require__(9);

var _axios2 = _interopRequireDefault(_axios);

var _authActionTypes = __webpack_require__(23);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var signUp = exports.signUp = function signUp(signData, callback) {
  return function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(dispatch) {
      var response;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return _axios2.default.post('/auth/signup', signData);

            case 3:
              response = _context.sent;

              if (response.data.errorLogin) {
                dispatch({ type: _authActionTypes.ERROR_LOGIN, payload: response.data.errorLogin });
              } else if (response.data.errorPassword) {
                dispatch({ type: _authActionTypes.ERROR_PASSWORD, payload: response.data.errorPassword });
              } else {
                dispatch({ type: _authActionTypes.AUTH_USER, payload: response.data });
              }
              if (callback) {
                callback();
              }
              _context.next = 11;
              break;

            case 8:
              _context.prev = 8;
              _context.t0 = _context['catch'](0);

              dispatch({ type: _authActionTypes.ERROR_LOGIN, payload: 'Some unexpected error happened' });

            case 11:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined, [[0, 8]]);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }();
};

var signIn = exports.signIn = function signIn(signData, callback) {
  return function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(dispatch) {
      var response;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.prev = 0;
              _context2.next = 3;
              return _axios2.default.post('/auth/signin', signData);

            case 3:
              response = _context2.sent;

              if (!response.data.error) {
                dispatch({ type: _authActionTypes.AUTH_USER, payload: response.data });
              } else {
                dispatch({ type: _authActionTypes.ERROR_LOGIN, payload: response.data.error });
              }
              if (callback) {
                callback();
              }
              _context2.next = 11;
              break;

            case 8:
              _context2.prev = 8;
              _context2.t0 = _context2['catch'](0);

              dispatch({ type: _authActionTypes.ERROR_LOGIN, payload: 'Some unexpected error happened' });

            case 11:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined, [[0, 8]]);
    }));

    return function (_x2) {
      return _ref2.apply(this, arguments);
    };
  }();
};

var signOut = exports.signOut = function signOut() {
  _axios2.default.post('/auth/session/delete');
  return {
    type: _authActionTypes.AUTH_USER,
    payload: null
  };
};

var resetErrors = exports.resetErrors = function resetErrors() {
  return { type: _authActionTypes.RESET_ERRORS };
};

function toggleModal() {
  return function (dispatch) {
    dispatch({
      type: _authActionTypes.TOGGLE_MODAL
    });
  };
}

var getUser = exports.getUser = function getUser() {
  return function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(dispatch) {
      var response;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.prev = 0;
              _context3.next = 3;
              return _axios2.default.post('/auth/session/get');

            case 3:
              response = _context3.sent;

              dispatch({ type: _authActionTypes.AUTH_USER, payload: response.data });
              _context3.next = 10;
              break;

            case 7:
              _context3.prev = 7;
              _context3.t0 = _context3['catch'](0);

              dispatch({ type: _authActionTypes.ERROR_LOGIN, payload: null });

            case 10:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, undefined, [[0, 7]]);
    }));

    return function (_x3) {
      return _ref3.apply(this, arguments);
    };
  }();
};

var signOutUser = exports.signOutUser = function signOutUser() {
  return function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(dispatch) {
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              try {
                _axios2.default.post('/auth/session/delete');
              } catch (e) {
                dispatch({ type: _authActionTypes.ERROR_LOGIN, payload: 'error' });
              }

            case 1:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, undefined);
    }));

    return function (_x4) {
      return _ref4.apply(this, arguments);
    };
  }();
};

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(5);

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Input = function (_Component) {
  _inherits(Input, _Component);

  function Input() {
    _classCallCheck(this, Input);

    return _possibleConstructorReturn(this, (Input.__proto__ || Object.getPrototypeOf(Input)).apply(this, arguments));
  }

  _createClass(Input, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var inputElement = this.props.type === 'text' ? _react2.default.createElement('input', {
        className: 'input ' + this.props.subClass,
        type: 'text',
        placeholder: ' ',
        onChange: this.props.onChange,
        value: this.props.value,
        ref: function ref(input) {
          _this2.input = input;
        }
      }) : _react2.default.createElement('textarea', {
        className: (0, _classnames2.default)('input ' + this.props.subClass, {
          filled: this.props.value !== ''
        }),
        onChange: this.props.onChange,
        value: this.props.value,
        ref: function ref(input) {
          _this2.input = input;
        }
      });

      return _react2.default.createElement(
        'div',
        { className: 'input-holder ' + this.props.class },
        inputElement,
        _react2.default.createElement(
          'div',
          {
            className: 'placeholder',
            onClick: function onClick() {
              _this2.input.focus();
            }
          },
          this.props.placeholder
        ),
        this.props.error !== '' && _react2.default.createElement(
          'div',
          { className: 'error' },
          this.props.error
        )
      );
    }
  }]);

  return Input;
}(_react.Component);

Input.propTypes = {
  onChange: _propTypes2.default.func,
  value: _propTypes2.default.string,
  subClass: _propTypes2.default.string,
  class: _propTypes2.default.string,
  type: _propTypes2.default.string,
  placeholder: _propTypes2.default.string,
  error: _propTypes2.default.string
};

exports.default = Input;

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _connectMongo = __webpack_require__(20);

var _connectMongo2 = _interopRequireDefault(_connectMongo);

var _expressSession = __webpack_require__(21);

var _expressSession2 = _interopRequireDefault(_expressSession);

var _mongoose = __webpack_require__(2);

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MongoStore = (0, _connectMongo2.default)(_expressSession2.default);

var useSession = function useSession(app) {
  app.use((0, _expressSession2.default)({
    secret: 'mySecretString',
    saveUninitialized: false,
    resave: false,
    cookie: { maxAge: 1000 * 60 * 60 * 24 * 2 },
    store: new MongoStore({
      mongooseConnection: _mongoose2.default.connection,
      ttl: 2 * 24 * 60 * 60
    })
  }));
};

exports.default = useSession;

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = require("connect-mongo");

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = require("express-session");

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _App = __webpack_require__(32);

var _App2 = _interopRequireDefault(_App);

var _Posts = __webpack_require__(47);

var _Posts2 = _interopRequireDefault(_Posts);

var _Contact = __webpack_require__(50);

var _Contact2 = _interopRequireDefault(_Contact);

var _AddPost = __webpack_require__(51);

var _AddPost2 = _interopRequireDefault(_AddPost);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = [_extends({}, _App2.default, {
  routes: [_extends({}, _Posts2.default, {
    exact: true,
    path: '/'
  }), _extends({}, _AddPost2.default, {
    exact: true,
    path: '/add'
  }), _extends({}, _Contact2.default, {
    exact: true,
    path: '/contact'
  })]
})];

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var AUTH_USER = exports.AUTH_USER = 'AUTH_USER';
var TOGGLE_MODAL = exports.TOGGLE_MODAL = 'TOGGLE_MODAL';
var ERROR_LOGIN = exports.ERROR_LOGIN = 'ERROR_LOGIN';
var ERROR_PASSWORD = exports.ERROR_PASSWORD = 'ERROR_PASSWORD';
var RESET_ERRORS = exports.RESET_ERRORS = 'RESET_ERRORS';

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var GET_DUMMY_POSTS = exports.GET_DUMMY_POSTS = 'GET_DUMMY_POSTS';
var GET_POST_COUNT = exports.GET_POST_COUNT = 'GET_POST_COUNT';
var REPLACE_DUMMY_POSTS = exports.REPLACE_DUMMY_POSTS = 'REPLACE_DUMMY_POSTS';
var UNSHIFT_POST = exports.UNSHIFT_POST = 'UNSHIFT_POST';
var RESET_POSTS = exports.RESET_POSTS = 'RESET_POSTS';
var GET_MAIN_POST = exports.GET_MAIN_POST = 'GET_MAIN_POST';
var RESET_POST = exports.RESET_POST = 'RESET_POST';
var ADD_COMMENT = exports.ADD_COMMENT = 'ADD_COMMENT';

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(5);

var _classnames2 = _interopRequireDefault(_classnames);

var _fadeTransition = __webpack_require__(11);

var _fadeTransition2 = _interopRequireDefault(_fadeTransition);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LoaderComponent = function (_Component) {
  _inherits(LoaderComponent, _Component);

  function LoaderComponent() {
    _classCallCheck(this, LoaderComponent);

    return _possibleConstructorReturn(this, (LoaderComponent.__proto__ || Object.getPrototypeOf(LoaderComponent)).apply(this, arguments));
  }

  _createClass(LoaderComponent, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        {
          className: (0, _classnames2.default)('loader-wrapper', {
            small: this.props.small
          })
        },
        !this.props.small && _react2.default.createElement(
          'h3',
          { className: 'loader-text' },
          'Give us a moment....'
        ),
        _react2.default.createElement('div', {
          className: (0, _classnames2.default)('loader loader--circularSquare', {
            small: this.props.small
          })
        })
      );
    }
  }]);

  return LoaderComponent;
}(_react.Component);

LoaderComponent.propTypes = {
  small: _propTypes2.default.bool
};

exports.default = (0, _fadeTransition2.default)(LoaderComponent);

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = __webpack_require__(2);

var _mongoose2 = _interopRequireDefault(_mongoose);

var _bcryptNodejs = __webpack_require__(66);

var _bcryptNodejs2 = _interopRequireDefault(_bcryptNodejs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var userSchema = new Schema({
  email: { type: String, unique: true, lowercase: true },
  password: String,
  isAdmin: { type: Boolean, default: false }
});

userSchema.pre('save', function (next) {
  // get access to the user model
  var user = this;

  // generate a salt then run callback
  _bcryptNodejs2.default.genSalt(10, function (err, salt) {
    if (err) {
      return next(err);
    }

    // hash (encrypt) our password using the salt
    _bcryptNodejs2.default.hash(user.password, salt, null, function (err, hash) {
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.comparePassword = function (candidatePassword, callback) {
  _bcryptNodejs2.default.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) {
      return callback(err);
    }

    callback(null, isMatch);
  });
};

var modelClass = _mongoose2.default.model('user', userSchema); //user - collection

exports.default = modelClass;

/***/ }),
/* 27 */,
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(13);

var _express = __webpack_require__(3);

var _express2 = _interopRequireDefault(_express);

var _httpProxy = __webpack_require__(29);

var _httpProxy2 = _interopRequireDefault(_httpProxy);

var _mongoose = __webpack_require__(2);

var _mongoose2 = _interopRequireDefault(_mongoose);

var _reactRouterConfig = __webpack_require__(14);

var _bodyParser = __webpack_require__(15);

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _morgan = __webpack_require__(16);

var _morgan2 = _interopRequireDefault(_morgan);

var _renderer = __webpack_require__(30);

var _renderer2 = _interopRequireDefault(_renderer);

var _createStore = __webpack_require__(58);

var _createStore2 = _interopRequireDefault(_createStore);

var _Routes = __webpack_require__(22);

var _Routes2 = _interopRequireDefault(_Routes);

var _authRoutes = __webpack_require__(63);

var _authRoutes2 = _interopRequireDefault(_authRoutes);

var _useSession = __webpack_require__(19);

var _useSession2 = _interopRequireDefault(_useSession);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.connect('mongodb://kreha:cms123@ds145562.mlab.com:45562/cms');
var app = (0, _express2.default)();
var apiProxy = _httpProxy2.default.createProxyServer({ target: 'http://localhost:3001' });

app.use('/api', function (req, res) {
  apiProxy.web(req, res);
});

app.use(_express2.default.static('public'));
app.use((0, _morgan2.default)('dev'));
app.use(_bodyParser2.default.json({ type: '*/*' }));
app.use(_bodyParser2.default.urlencoded({ extended: false }));

(0, _useSession2.default)(app);

app.get('*', function (req, res) {
  //req does contain cookies so I need it inside createStore
  var store = (0, _createStore2.default)(req);

  var promises = (0, _reactRouterConfig.matchRoutes)(_Routes2.default, req.path).map(function (_ref) {
    var route = _ref.route;

    return route.loadData ? route.loadData(store) : null;
  }).map(function (promise) {
    if (promise) {
      return new Promise(function (resolve, reject) {
        //this new Promise wrapping original one is going to be resolved no matter what
        promise.then(resolve).catch(resolve);
      });
    }
  });

  //connecting all the promises into one
  Promise.all(promises).then(function () {
    var context = {};

    var content = (0, _renderer2.default)(req, store, context);

    if (context.url) {
      return res.redirect(301, context.url);
    }
    if (context.notFound) {
      res.status(404);
    }

    res.send(content);
  });
});

app.use('/auth', _authRoutes2.default);

app.listen(3000, function (err) {
  if (err) {
    console.log(err);
  }
  console.log('main server on port 3000');
});

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = require("http-proxy");

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _server = __webpack_require__(31);

var _reactRouterDom = __webpack_require__(7);

var _Routes = __webpack_require__(22);

var _Routes2 = _interopRequireDefault(_Routes);

var _reactRedux = __webpack_require__(4);

var _reactRouterConfig = __webpack_require__(14);

var _serializeJavascript = __webpack_require__(57);

var _serializeJavascript2 = _interopRequireDefault(_serializeJavascript);

var _reactHelmet = __webpack_require__(12);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//protects against potential xss attacks, it replaces things like < or > with unicode
exports.default = function (req, store, context) {
  var content = (0, _server.renderToString)(_react2.default.createElement(
    _reactRedux.Provider,
    { store: store },
    _react2.default.createElement(
      _reactRouterDom.StaticRouter,
      { location: req.url, context: context },
      _react2.default.createElement(
        'div',
        null,
        (0, _reactRouterConfig.renderRoutes)(_Routes2.default)
      )
    )
  ));

  var helmet = _reactHelmet.Helmet.renderStatic();

  return '\n    <html>\n      <head>\n        ' + helmet.title.toString() + '\n        ' + helmet.meta.toString() + '\n        <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">\n        <link rel="stylesheet" type="text/css" href="bundle.css">\n        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">\n        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">\n        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">\n      </head>\n      <body>\n        <div id="root">' + content + '</div>\n        <script>\n          window.INITIAL_STATE = ' + (0, _serializeJavascript2.default)(store.getState()) + '\n        </script>\n        <script src="bundle.js"></script>\n      </body>\n    </html>\n  ';
};

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = require("react-dom/server");

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactRouterConfig = __webpack_require__(14);

var _Navbar = __webpack_require__(33);

var _Navbar2 = _interopRequireDefault(_Navbar);

var _reactRedux = __webpack_require__(4);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRouterDom = __webpack_require__(7);

var _redux = __webpack_require__(6);

var _authActions = __webpack_require__(17);

var authActions = _interopRequireWildcard(_authActions);

var _postActions = __webpack_require__(10);

var postActions = _interopRequireWildcard(_postActions);

var _SignModal = __webpack_require__(34);

var _SignModal2 = _interopRequireDefault(_SignModal);

var _Loader = __webpack_require__(25);

var _Loader2 = _interopRequireDefault(_Loader);

__webpack_require__(46);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var App = function (_Component) {
  _inherits(App, _Component);

  function App(props) {
    _classCallCheck(this, App);

    var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, props));

    _this.toggleNav = function () {
      _this.setState({
        navOpen: !_this.state.navOpen
      });
    };

    _this.toggleLoader = function () {
      _this.setState({
        loader: !_this.state.loader
      });
    };

    _this.disableLoader = function () {
      _this.setState({
        loader: false
      });
    };

    _this.state = { navOpen: false, loader: false };
    return _this;
  }

  _createClass(App, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'main',
        { className: 'main' },
        _react2.default.createElement(
          'h1',
          { className: 'main-title' },
          'SomeFancyTitleForMyBlog ;)'
        ),
        this.state.loader && _react2.default.createElement(_Loader2.default, { small: false }),
        _react2.default.createElement(_SignModal2.default, { open: this.props.auth.modalOpen }),
        _react2.default.createElement(_Navbar2.default, {
          toggleNav: this.toggleNav,
          navOpen: this.state.navOpen,
          toggleSignModal: this.props.authActions.toggleModal
        }),
        _react2.default.createElement(
          'section',
          { className: 'content' },
          (0, _reactRouterConfig.renderRoutes)(this.props.route.routes, {
            toggleLoader: this.toggleLoader,
            disableLoader: this.disableLoader
          })
        )
      );
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.props.authActions.getUser();
      this.props.postActions.getPostsCount();
    }
  }]);

  return App;
}(_react.Component);

App.propTypes = {
  route: _propTypes2.default.object,
  authActions: _propTypes2.default.object,
  postActions: _propTypes2.default.object,
  auth: _propTypes2.default.object
};

function mapStateToProps(state) {
  return { auth: state.auth };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: (0, _redux.bindActionCreators)(authActions, dispatch),
    postActions: (0, _redux.bindActionCreators)(postActions, dispatch)
  };
}

exports.default = {
  component: (0, _reactRouterDom.withRouter)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(App)),
  loadData: function loadData(_ref) {
    var dispatch = _ref.dispatch;
    return dispatch(authActions.getUser());
  }
};

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = __webpack_require__(7);

var _classnames = __webpack_require__(5);

var _classnames2 = _interopRequireDefault(_classnames);

var _reactRedux = __webpack_require__(4);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(6);

var _authActions = __webpack_require__(17);

var authActions = _interopRequireWildcard(_authActions);

var _postActions = __webpack_require__(10);

var postActions = _interopRequireWildcard(_postActions);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Navbar = function (_Component) {
  _inherits(Navbar, _Component);

  function Navbar() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Navbar);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Navbar.__proto__ || Object.getPrototypeOf(Navbar)).call.apply(_ref, [this].concat(args))), _this), _this.openModalAndCloseNav = function (isSignedIn) {
      if (isSignedIn) {
        _this.props.authActions.signOut();
      } else {
        _this.props.toggleNav();
        _this.props.authActions.toggleModal();
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Navbar, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var isSignedIn = !!this.props.auth.authenticated && !!this.props.auth.authenticated.user;
      var isAdmin = isSignedIn && this.props.auth.authenticated.isAdmin;
      var isMainSite = this.props.location.pathname === '/';
      return _react2.default.createElement(
        'div',
        {
          id: 'circularMenu1',
          className: (0, _classnames2.default)({
            'circular-menu circular-menu-left': true,
            active: this.props.navOpen,
            admin: isAdmin
          })
        },
        _react2.default.createElement(
          'a',
          {
            className: 'floating-btn',
            onClick: function onClick() {
              _this2.props.toggleNav();
              if (!isMainSite) {
                _this2.props.postActions.resetPosts();
              }
            }
          },
          _react2.default.createElement('i', { className: 'fa fa-bars' })
        ),
        _react2.default.createElement(
          'menu',
          { className: 'items-wrapper' },
          _react2.default.createElement(_reactRouterDom.Link, {
            to: '/',
            className: 'menu-item fa fa-home',
            onClick: function onClick() {
              _this2.props.toggleNav();
              if (!isMainSite) {
                _this2.props.postActions.resetPosts();
              }
            }
          }),
          _react2.default.createElement('a', {
            href: '#',
            className: (0, _classnames2.default)({
              'menu-item fa': true,
              'fa-sign-in': !isSignedIn,
              'fa-sign-out': isSignedIn
            }),
            onClick: function onClick() {
              return _this2.openModalAndCloseNav(isSignedIn);
            }
          }),
          _react2.default.createElement(_reactRouterDom.Link, {
            to: '/contact',
            className: 'menu-item fa fa-envelope-o',
            onClick: function onClick() {
              _this2.props.toggleNav();
              if (!isMainSite) {
                _this2.props.postActions.resetPosts();
              }
            }
          }),
          isAdmin && _react2.default.createElement(_reactRouterDom.Link, {
            to: '/add',
            className: 'menu-item fa fa-plus-circle',
            onClick: function onClick() {
              _this2.props.toggleNav();
              if (!isMainSite) {
                _this2.props.postActions.resetPosts();
              }
            }
          })
        )
      );
    }
  }]);

  return Navbar;
}(_react.Component);

Navbar.propTypes = {
  toggleNav: _propTypes2.default.func,
  authActions: _propTypes2.default.object,
  postActions: _propTypes2.default.object,
  auth: _propTypes2.default.object,
  location: _propTypes2.default.object,
  navOpen: _propTypes2.default.bool
};

function mapStateToProps(state) {
  return { auth: state.auth };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: (0, _redux.bindActionCreators)(authActions, dispatch),
    postActions: (0, _redux.bindActionCreators)(postActions, dispatch)
  };
}

exports.default = (0, _reactRouterDom.withRouter)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(Navbar));

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _Button = __webpack_require__(35);

var _Button2 = _interopRequireDefault(_Button);

var _TextField = __webpack_require__(36);

var _TextField2 = _interopRequireDefault(_TextField);

var _Dialog = __webpack_require__(37);

var _Dialog2 = _interopRequireDefault(_Dialog);

var _DialogActions = __webpack_require__(38);

var _DialogActions2 = _interopRequireDefault(_DialogActions);

var _DialogContent = __webpack_require__(39);

var _DialogContent2 = _interopRequireDefault(_DialogContent);

var _Visibility = __webpack_require__(40);

var _Visibility2 = _interopRequireDefault(_Visibility);

var _VisibilityOff = __webpack_require__(41);

var _VisibilityOff2 = _interopRequireDefault(_VisibilityOff);

var _IconButton = __webpack_require__(42);

var _IconButton2 = _interopRequireDefault(_IconButton);

var _InputAdornment = __webpack_require__(43);

var _InputAdornment2 = _interopRequireDefault(_InputAdornment);

var _DialogTitle = __webpack_require__(44);

var _DialogTitle2 = _interopRequireDefault(_DialogTitle);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__(4);

var _redux = __webpack_require__(6);

var _authActions = __webpack_require__(17);

var authActions = _interopRequireWildcard(_authActions);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SignModal = function (_React$Component) {
  _inherits(SignModal, _React$Component);

  function SignModal() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, SignModal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = SignModal.__proto__ || Object.getPrototypeOf(SignModal)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      inputs: {
        email: '',
        password: '',
        passwordCheck: ''
      },
      modalTypeIn: true,
      showPassword: false
    }, _this.handleChange = function (propertyName) {
      return function (e) {
        var inputs = _this.state.inputs;


        inputs[propertyName] = e.target.value;
        _this.props.authActions.resetErrors();
        _this.setState({ inputs: inputs });
      };
    }, _this.changeModalType = function () {
      _this.setState({ modalTypeIn: !_this.state.modalTypeIn });
    }, _this.toggleShowPassword = function () {
      _this.setState({ showPassword: !_this.state.showPassword });
    }, _this.submitForm = function () {
      var _this$state$inputs = _this.state.inputs,
          email = _this$state$inputs.email,
          password = _this$state$inputs.password,
          passwordCheck = _this$state$inputs.passwordCheck;

      if (_this.state.modalTypeIn) {
        _this.props.authActions.signIn({
          email: email,
          password: password
        });
      } else {
        _this.props.authActions.signUp({
          email: email,
          password: password,
          passwordCheck: passwordCheck
          // , () => {
          //   this.props.history.push('/tajnasciezka'); moge tego uzyc w akcji i od razu przeniesc ziomka gdzies
          // }
        });
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(SignModal, [{
    key: 'render',
    value: function render() {
      var isSignInModal = this.state.modalTypeIn;
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _Dialog2.default,
          {
            open: this.props.open,
            onClose: this.props.authActions.toggleModal,
            'aria-labelledby': 'form-dialog-title'
          },
          _react2.default.createElement(
            _DialogTitle2.default,
            { id: 'form-dialog-title' },
            isSignInModal ? 'Sign in to existing account' : 'Create new account'
          ),
          _react2.default.createElement(
            _DialogContent2.default,
            null,
            _react2.default.createElement(_TextField2.default, {
              error: !!this.props.auth.errorLogin,
              autoFocus: true,
              margin: 'dense',
              id: 'email',
              label: this.props.auth.errorLogin ? this.props.auth.errorLogin : 'Email Address',
              type: 'email',
              fullWidth: true,
              value: this.state.inputs.email,
              onChange: this.handleChange('email')
            }),
            _react2.default.createElement(_TextField2.default, {
              error: !!this.props.auth.errorPassword,
              margin: 'dense',
              fullWidth: true,
              type: this.state.showPassword ? 'text' : 'password',
              id: 'pass',
              label: this.props.auth.errorPassword ? this.props.auth.errorPassword : 'Password',
              value: this.state.inputs.password,
              onChange: this.handleChange('password'),
              InputProps: {
                endAdornment: _react2.default.createElement(
                  _InputAdornment2.default,
                  { variant: 'filled', position: 'end' },
                  _react2.default.createElement(
                    _IconButton2.default,
                    {
                      'aria-label': 'Toggle password visibility',
                      onClick: this.toggleShowPassword
                    },
                    this.state.showPassword ? _react2.default.createElement(_VisibilityOff2.default, null) : _react2.default.createElement(_Visibility2.default, null)
                  )
                )
              }
            }),
            !isSignInModal && _react2.default.createElement(_TextField2.default, {
              margin: 'dense',
              fullWidth: true,
              type: this.state.showPassword ? 'text' : 'password',
              id: 'passCheck',
              label: 'Retype your password',
              value: this.state.inputs.passwordCheck,
              onChange: this.handleChange('passwordCheck'),
              InputProps: {
                endAdornment: _react2.default.createElement(
                  _InputAdornment2.default,
                  { variant: 'filled', position: 'end' },
                  _react2.default.createElement(
                    _IconButton2.default,
                    {
                      'aria-label': 'Toggle password visibility',
                      onClick: this.toggleShowPassword
                    },
                    this.state.showPassword ? _react2.default.createElement(_VisibilityOff2.default, null) : _react2.default.createElement(_Visibility2.default, null)
                  )
                )
              }
            })
          ),
          _react2.default.createElement(
            _DialogActions2.default,
            null,
            _react2.default.createElement(
              _Button2.default,
              { onClick: this.changeModalType, color: 'primary' },
              isSignInModal ? 'I need new account' : 'I already have an account'
            ),
            _react2.default.createElement(
              _Button2.default,
              {
                onClick: this.props.authActions.toggleModal,
                color: 'primary'
              },
              'Cancel'
            ),
            _react2.default.createElement(
              _Button2.default,
              { onClick: this.submitForm, color: 'primary' },
              isSignInModal ? 'Sign in' : 'Create new account'
            )
          )
        )
      );
    }
  }]);

  return SignModal;
}(_react2.default.Component);

SignModal.propTypes = {
  open: _propTypes2.default.bool,
  authActions: _propTypes2.default.object,
  modalType: _propTypes2.default.string,
  auth: _propTypes2.default.object
};

function mapStateToProps(state) {
  return { auth: state.auth };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: (0, _redux.bindActionCreators)(authActions, dispatch)
  };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(SignModal);

/***/ }),
/* 35 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Button");

/***/ }),
/* 36 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/TextField");

/***/ }),
/* 37 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Dialog");

/***/ }),
/* 38 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/DialogActions");

/***/ }),
/* 39 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/DialogContent");

/***/ }),
/* 40 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Visibility");

/***/ }),
/* 41 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/VisibilityOff");

/***/ }),
/* 42 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/IconButton");

/***/ }),
/* 43 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/InputAdornment");

/***/ }),
/* 44 */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/DialogTitle");

/***/ }),
/* 45 */
/***/ (function(module, exports) {

module.exports = require("react-addons-css-transition-group");

/***/ }),
/* 46 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactRedux = __webpack_require__(4);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactHelmet = __webpack_require__(12);

var _reactRouterDom = __webpack_require__(7);

var _redux = __webpack_require__(6);

var _postActions = __webpack_require__(10);

var postActions = _interopRequireWildcard(_postActions);

var _PostListElement = __webpack_require__(48);

var _PostListElement2 = _interopRequireDefault(_PostListElement);

var _Post = __webpack_require__(49);

var _Post2 = _interopRequireDefault(_Post);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Posts = function (_Component) {
  _inherits(Posts, _Component);

  function Posts(props) {
    _classCallCheck(this, Posts);

    var _this = _possibleConstructorReturn(this, (Posts.__proto__ || Object.getPrototypeOf(Posts)).call(this, props));

    _this.getPosts = function () {
      var postsToGet = Math.min(9, _this.props.post.count - _this.props.post.posts.length);
      _this.props.postActions.createDummyPosts(postsToGet);
      _this.props.postActions.getPosts(_this.props.post.posts.length);
    };

    _this.getPost = function (id) {
      _this.props.postActions.getPost(id);
    };

    _this.state = {};
    return _this;
  }

  _createClass(Posts, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var postData = this.props.post;
      var addMoreButton = postData.posts.length < this.props.post.count;
      var posts = [];
      if (postData.posts.length > 0) {
        posts = postData.posts.map(function (el) {
          return _react2.default.createElement(_PostListElement2.default, {
            post: el,
            key: el._id,
            getPost: function getPost() {
              _this2.props.toggleLoader();
              _this2.getPost(el._id);
              _this2.props.disableLoader();
            }
          });
        });
      }
      return _react2.default.createElement(
        'section',
        { className: 'post-list' },
        _react2.default.createElement(
          _reactHelmet.Helmet,
          null,
          _react2.default.createElement(
            'title',
            null,
            'Check out my posts'
          )
        ),
        postData.selectedPost && _react2.default.createElement(_Post2.default, {
          post: postData.selectedPost,
          auth: this.props.auth,
          close: this.props.postActions.resetPost,
          addComment: this.props.postActions.addComment
        }),
        _react2.default.createElement(
          'div',
          { className: 'section-title' },
          _react2.default.createElement(
            'h1',
            null,
            'Check out my posts ;)'
          )
        ),
        _react2.default.createElement(
          'ul',
          { className: 'posts-list__list' },
          posts,
          addMoreButton && _react2.default.createElement(
            'div',
            { className: 'button-wrapper normal' },
            _react2.default.createElement(
              'button',
              { className: 'btn-send active', onClick: this.getPosts },
              'Get more posts'
            )
          )
        )
      );
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.props.post.posts.length === 0 && this.props.post.count > this.props.post.posts.length) {
        var postsToGet = Math.min(9, this.props.post.count - this.props.post.posts.length);
        this.props.postActions.createDummyPosts(postsToGet);
        this.props.postActions.getPosts(this.props.post.posts.length);
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.props.postActions.getPostsCount();
    }
  }]);

  return Posts;
}(_react.Component);

Posts.propTypes = {
  postActions: _propTypes2.default.object,
  post: _propTypes2.default.object,
  auth: _propTypes2.default.object,
  toggleLoader: _propTypes2.default.func,
  disableLoader: _propTypes2.default.func
};

function mapStateToProps(state) {
  return { post: state.post, auth: state.auth };
}

function mapDispatchToProps(dispatch) {
  return {
    postActions: (0, _redux.bindActionCreators)(postActions, dispatch)
  };
}

exports.default = {
  component: (0, _reactRouterDom.withRouter)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(Posts)),
  loadData: function loadData(_ref) {
    var dispatch = _ref.dispatch;
    return dispatch(postActions.getPostsCount());
  }
};

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _fadeTransition = __webpack_require__(11);

var _fadeTransition2 = _interopRequireDefault(_fadeTransition);

var _Loader = __webpack_require__(25);

var _Loader2 = _interopRequireDefault(_Loader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PostListElement = function PostListElement(props) {
  if (props.post.loader) {
    return _react2.default.createElement(
      'div',
      { className: 'posts-list__list__list-element' },
      _react2.default.createElement(_Loader2.default, { small: true })
    );
  }
  var subtitle = null;
  if (props.post.subTitle) {
    if (props.post.subTitle.length > 30) {
      subtitle = props.post.subTitle.substring(0, 30) + '...';
    } else {
      subtitle = props.post.subTitle;
    }
  }
  return _react2.default.createElement(
    'div',
    {
      onClick: props.getPost,
      className: 'posts-list__list__list-element loaded'
    },
    _react2.default.createElement(
      'h4',
      { className: 'posts-list__list__list-element__title' },
      props.post.title
    ),
    _react2.default.createElement('img', {
      className: 'posts-list__list__list-element__img',
      src: props.post.img
    }),
    _react2.default.createElement(
      'div',
      { className: 'sub-data' },
      _react2.default.createElement(
        'p',
        { className: 'posts-list__list__list-element__subTitle' },
        subtitle
      ),
      _react2.default.createElement(
        'p',
        null,
        'Posted: ',
        props.post.date
      )
    )
  );
};

PostListElement.propTypes = {
  postActions: _propTypes2.default.object,
  post: _propTypes2.default.object,
  getPost: _propTypes2.default.func
};

exports.default = (0, _fadeTransition2.default)(PostListElement);

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(5);

var _classnames2 = _interopRequireDefault(_classnames);

var _fadeTransition = __webpack_require__(11);

var _fadeTransition2 = _interopRequireDefault(_fadeTransition);

var _Input = __webpack_require__(18);

var _Input2 = _interopRequireDefault(_Input);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Post = function (_Component) {
  _inherits(Post, _Component);

  function Post(props) {
    _classCallCheck(this, Post);

    var _this = _possibleConstructorReturn(this, (Post.__proto__ || Object.getPrototypeOf(Post)).call(this, props));

    _this.onSubmit = function () {
      var today = new Date();
      var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
      var minutes = today.getMinutes();
      var seconds = today.getSeconds();
      var time = today.getHours() + ':' + ('' + (minutes.length === 1 ? '0' : '') + minutes) + ':' + ('' + (seconds.length === 1 ? '0' : '') + minutes);
      var dateTime = date + ' ' + time;
      var commentData = {
        comment: {
          date: dateTime,
          content: _this.state.comment,
          author: _this.props.auth.authenticated.user
        },
        postId: _this.props.post._id
      };
      _this.props.addComment(commentData);
      _this.setState({ comment: '', error: '' });
    };

    _this.onCommentChange = function (e) {
      _this.setState({ comment: e.target.value });
    };

    _this.state = {
      hasOveflow: false,
      comment: '',
      error: ''
    };
    return _this;
  }

  _createClass(Post, [{
    key: 'render',
    value: function render() {
      var isAuthenticated = this.props.auth.authenticated && this.props.auth.authenticated.user && this.props.auth.authenticated.token;
      var addComment = isAuthenticated ? _react2.default.createElement(
        'div',
        { className: 'add-comment' },
        _react2.default.createElement(_Input2.default, {
          'class': 'content-input',
          subClass: 'input--content textarea',
          type: 'textarea',
          placeholder: 'Add new comment',
          onChange: this.onCommentChange,
          value: this.state.comment,
          error: this.state.error
        }),
        _react2.default.createElement(
          'div',
          { className: 'button-wrapper' },
          _react2.default.createElement(
            'button',
            {
              className: (0, _classnames2.default)('btn-send', { active: this.state.comment !== '' }, { error: this.state.error }),
              onClick: this.onSubmit
            },
            this.state.error ? 'Please try again' : 'Add new comment'
          )
        )
      ) : _react2.default.createElement(
        'p',
        { className: 'info' },
        'You need to log in if you want to add new comment'
      );
      var comments = null;
      if (this.props.post.comments && this.props.post.comments.length > 0) {
        comments = this.props.post.comments.map(function (el) {
          return _react2.default.createElement(
            'li',
            { className: 'comment', key: el._id },
            _react2.default.createElement(
              'h6',
              { className: 'comment-author' },
              _react2.default.createElement('a', { className: 'fa fa-user-circle-o' }),
              el.author
            ),
            _react2.default.createElement(
              'p',
              { className: 'comment-content' },
              el.content
            ),
            _react2.default.createElement(
              'p',
              { className: 'comment-date' },
              el.date
            )
          );
        });
      }
      var customs = null;

      if (this.props.post.customs && this.props.post.customs.length > 0) {
        customs = this.props.post.customs.map(function (el, i) {
          if (el.elType === 'p') {
            return _react2.default.createElement(
              'li',
              { className: 'p', key: i },
              el.title && _react2.default.createElement(
                'h6',
                null,
                el.title
              ),
              _react2.default.createElement(
                'p',
                null,
                el.content
              )
            );
          } else {
            return _react2.default.createElement(
              'li',
              { className: 'img', key: i },
              _react2.default.createElement('img', { src: el.image }),
              el.content && _react2.default.createElement(
                'h6',
                null,
                el.content
              )
            );
          }
        });
      }
      return _react2.default.createElement(
        'div',
        { className: 'posts-list__main-post' },
        this.state.hasOveflow && _react2.default.createElement('a', { className: 'fa fa-get-pocket scroll-sign scroll-up' }),
        this.state.hasOveflow && _react2.default.createElement('a', { className: 'fa fa-get-pocket scroll-sign scroll-down' }),
        _react2.default.createElement('div', { className: 'background', onClick: this.props.close }),
        _react2.default.createElement(
          'div',
          { id: 'post-content', className: 'content' },
          _react2.default.createElement(
            'div',
            { className: 'close-button', onClick: this.props.close },
            _react2.default.createElement('a', {
              href: '#',
              className: 'fa fa-times-circle',
              onClick: this.props.close
            })
          ),
          _react2.default.createElement(
            'h4',
            null,
            this.props.post.title
          ),
          _react2.default.createElement(
            'h5',
            null,
            this.props.post.subTitle
          ),
          _react2.default.createElement('img', { src: this.props.post.img }),
          _react2.default.createElement(
            'p',
            { className: 'paragraph' },
            this.props.post.content
          ),
          _react2.default.createElement(
            'ul',
            { className: 'customs' },
            customs
          ),
          addComment,
          _react2.default.createElement(
            'ul',
            { className: 'comments' },
            comments
          )
        )
      );
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var content = document.getElementById('post-content');
      if (content.scrollHeight > content.offsetHeight) {
        this.setState({ hasOveflow: true });
      }
    }
  }]);

  return Post;
}(_react.Component);

Post.propTypes = {
  postActions: _propTypes2.default.object,
  post: _propTypes2.default.object,
  auth: _propTypes2.default.object,
  close: _propTypes2.default.func,
  addComment: _propTypes2.default.func
};

exports.default = (0, _fadeTransition2.default)(Post);

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactHelmet = __webpack_require__(12);

var _reactRedux = __webpack_require__(4);

var _classnames = __webpack_require__(5);

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _axios = __webpack_require__(9);

var _axios2 = _interopRequireDefault(_axios);

var _Input = __webpack_require__(18);

var _Input2 = _interopRequireDefault(_Input);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Contact = function (_Component) {
  _inherits(Contact, _Component);

  function Contact(props) {
    _classCallCheck(this, Contact);

    var _this = _possibleConstructorReturn(this, (Contact.__proto__ || Object.getPrototypeOf(Contact)).call(this, props));

    _this.handleChange = function (propertyName) {
      return function (e) {
        var inputs = _this.state.inputs;
        var errors = _this.state.errors;


        inputs[propertyName] = e.target.value;
        errors[propertyName] = '';

        _this.setState({ inputs: inputs, errors: errors, error: false });
      };
    };

    _this.onSubmit = function () {
      if (!_this.state.error) {
        _this.setState({ error: false });
      }
      var inputs = _this.state.inputs;

      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      var isEmailValid = re.test(String(inputs.contact).toLowerCase());
      if (isEmailValid) {
        _this.props.toggleLoader();
        _axios2.default.post('/api/contact', _this.state.inputs).then(function (response) {
          if (response.statusText !== 'OK') {
            _this.setState({
              errors: _extends({}, _this.state.errors, { message: response.data.error }),
              error: true
            });
          }
          _this.props.toggleLoader();
          _this.setState({
            inputs: {
              subject: '',
              message: '',
              contact: ''
            }
          });
        });
      } else {
        if (!_this.state.error) {
          _this.setState({
            errors: {
              message: '',
              subject: '',
              contact: 'Are you sure this email is valid?'
            },
            error: true
          });
        }
      }
    };

    _this.checkForErrors = function () {
      var inputs = _this.state.inputs;

      var error = !inputs.subject || !inputs.contact || !inputs.message;
      var emailError = "Don't you want me to text you back? ;p";
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (inputs.contact) {
        var isEmailValid = re.test(String(inputs.contact).toLowerCase());
        emailError = isEmailValid ? '' : 'Are you sure this email is valid?';
      }
      var errors = {
        subject: inputs.title ? '' : 'Please add subject to your message',
        message: inputs.message ? '' : 'That should not be empty ;p',
        contact: inputs.contact ? '' : emailError
      };
      _this.setState({ error: error, errors: errors });
    };

    _this.onNewsSubmit = function () {
      var inputs = _this.state.inputs;

      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      var isEmailValid = re.test(String(inputs.email).toLowerCase());
      if (!isEmailValid) {
        _this.setState({
          errors: _extends({}, _this.state.errors, {
            email: 'Are you sure this email is valid?'
          })
        });
      } else {
        _this.props.toggleLoader();
        _axios2.default.post('/api/newsletter/add', { email: inputs.email }).then(function (response) {
          if (response.data.error) {
            _this.setState({
              errors: _extends({}, _this.state.errors, { email: response.data.error }),
              error: true
            });
          }
          _this.props.toggleLoader();
          _this.setState({
            inputs: {
              subject: '',
              message: '',
              contact: '',
              email: ''
            }
          });
        });
      }
    };

    _this.state = {
      inputs: {
        subject: '',
        message: '',
        contact: '',
        email: ''
      },
      errors: {
        subject: '',
        message: '',
        contact: '',
        email: ''
      },
      error: false
    };
    return _this;
  }

  _createClass(Contact, [{
    key: 'render',
    value: function render() {
      var _state = this.state,
          inputs = _state.inputs,
          errors = _state.errors;

      var isButtonDisabled = !inputs.subject || !inputs.contact || !inputs.message;
      return _react2.default.createElement(
        'section',
        { className: 'contact-page' },
        _react2.default.createElement(
          _reactHelmet.Helmet,
          null,
          _react2.default.createElement(
            'title',
            null,
            'Contact me'
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'section-title' },
          _react2.default.createElement(
            'h1',
            null,
            'Do you want to tell me something?'
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'left-side' },
          _react2.default.createElement(_Input2.default, {
            'class': 'title-input',
            subClass: 'input--title',
            type: 'text',
            onChange: this.handleChange('subject'),
            placeholder: 'Type in subject for your message',
            value: inputs.subject,
            error: errors.subject
          }),
          _react2.default.createElement(_Input2.default, {
            'class': 'title-input',
            subClass: 'input--title',
            type: 'text',
            onChange: this.handleChange('contact'),
            placeholder: 'How can I contact you? Type your email',
            value: inputs.contact,
            error: errors.contact
          }),
          _react2.default.createElement(_Input2.default, {
            'class': 'content-input',
            subClass: 'input--content textarea',
            type: 'textarea',
            placeholder: 'Your message',
            onChange: this.handleChange('message'),
            value: inputs.message,
            error: errors.message
          }),
          _react2.default.createElement(
            'div',
            { className: 'button-wrapper' },
            _react2.default.createElement(
              'button',
              {
                className: (0, _classnames2.default)('btn-send', { active: !isButtonDisabled }, { error: this.state.error }),
                onClick: isButtonDisabled ? this.checkForErrors : this.onSubmit
              },
              'Submit'
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'newsletter' },
            _react2.default.createElement(
              'div',
              { className: 'section-title' },
              _react2.default.createElement(
                'h1',
                null,
                'Maybe you want to receive newsletter?'
              )
            ),
            _react2.default.createElement(_Input2.default, {
              'class': 'title-input',
              subClass: 'input--title',
              type: 'text',
              onChange: this.handleChange('email'),
              placeholder: 'Type in your email',
              value: inputs.email,
              error: errors.email
            }),
            _react2.default.createElement(
              'div',
              { className: 'button-wrapper' },
              _react2.default.createElement(
                'button',
                {
                  className: (0, _classnames2.default)('btn-send', { active: inputs.email !== '' }, { error: errors.email !== '' }),
                  onClick: inputs.email === '' ? null : this.onNewsSubmit
                },
                'Submit'
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'right-side' },
          _react2.default.createElement(
            'h3',
            null,
            'Hi ;) I\'m glad you want to contact me. You can do that using this form. However if you prefer sending me email directly from your address you can send it to cmscontact1234@gmail.com. Cheers ;)'
          )
        )
      );
    }
  }]);

  return Contact;
}(_react.Component);

Contact.propTypes = {
  toggleLoader: _propTypes2.default.func
};

exports.default = {
  component: (0, _reactRedux.connect)(null, null)(Contact)
};

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactRedux = __webpack_require__(4);

var _classnames = __webpack_require__(5);

var _classnames2 = _interopRequireDefault(_classnames);

var _reactRouterDom = __webpack_require__(7);

var _reactHelmet = __webpack_require__(12);

var _axios = __webpack_require__(9);

var _axios2 = _interopRequireDefault(_axios);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(6);

var _postActions = __webpack_require__(10);

var postActions = _interopRequireWildcard(_postActions);

var _ComponentWrapper = __webpack_require__(52);

var _ComponentWrapper2 = _interopRequireDefault(_ComponentWrapper);

var _Success = __webpack_require__(53);

var _Success2 = _interopRequireDefault(_Success);

var _requireAdmin = __webpack_require__(54);

var _requireAdmin2 = _interopRequireDefault(_requireAdmin);

var _Dropzone = __webpack_require__(55);

var _Dropzone2 = _interopRequireDefault(_Dropzone);

var _Input = __webpack_require__(18);

var _Input2 = _interopRequireDefault(_Input);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var url = 'https://s3.eu-central-1.amazonaws.com/cms-bucket-06/';

var initialState = {
  success: '',
  mainPic: null,
  mainPicError: '',
  inputs: {
    content: '',
    title: '',
    subTitle: ''
  },
  customElements: [],
  nextCustomId: 1,
  errors: {
    content: '',
    title: '',
    image: '',
    button: ''
  },
  error: false
};

var AddPost = function (_Component) {
  _inherits(AddPost, _Component);

  function AddPost(props) {
    var _this2 = this;

    _classCallCheck(this, AddPost);

    var _this = _possibleConstructorReturn(this, (AddPost.__proto__ || Object.getPrototypeOf(AddPost)).call(this, props));

    _this.onDrop = function (acceptedFile) {
      _this.setState({
        mainPic: acceptedFile,
        mainPicError: '',
        error: false,
        errors: _extends({}, _this.state.errors, {
          button: ''
        })
      });
    };

    _this.handleChange = function (propertyName) {
      return function (e) {
        var inputs = _this.state.inputs;
        var errors = _this.state.errors;


        inputs[propertyName] = e.target.value;
        errors[propertyName] = '';

        _this.setState({ inputs: inputs, errors: _extends({}, errors, { button: '' }), error: false });
      };
    };

    _this.addNewParagraph = function () {
      _this.setState({
        success: '',
        nextCustomId: _this.state.nextCustomId + 1,
        customElements: [].concat(_toConsumableArray(_this.state.customElements), [{
          id: _this.state.nextCustomId,
          type: 'paragraph',
          content: '',
          title: '',
          error: ''
        }]),
        errors: _extends({}, _this.state.errors, {
          button: ''
        })
      });
    };

    _this.addNewImage = function () {
      _this.setState({
        nextCustomId: _this.state.nextCustomId + 1,
        customElements: [].concat(_toConsumableArray(_this.state.customElements), [{
          id: _this.state.nextCustomId,
          type: 'image',
          image: null,
          caption: '',
          error: ''
        }])
      });
    };

    _this.onSubmit = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
      var file, uploadConfig, customs;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _this.props.toggleLoader();
              file = _this.state.mainPic;
              _context3.next = 4;
              return _axios2.default.post('/api/img/upload', {
                ContentType: file.type
              });

            case 4:
              uploadConfig = _context3.sent;
              _context3.next = 7;
              return _axios2.default.put(uploadConfig.data.url, file, {
                headers: {
                  'Content-Type': file.type
                }
              });

            case 7:
              customs = _this.state.customElements.map(function () {
                var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(el) {
                  var elUploadConfig;
                  return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          if (!(el.type === 'paragraph')) {
                            _context.next = 4;
                            break;
                          }

                          return _context.abrupt('return', {
                            elType: 'p',
                            content: el.content,
                            title: el.title
                          });

                        case 4:
                          _context.next = 6;
                          return _axios2.default.post('/api/img/upload', {
                            ContentType: el.image.type
                          });

                        case 6:
                          elUploadConfig = _context.sent;
                          _context.next = 9;
                          return _axios2.default.put(elUploadConfig.data.url, el.image, {
                            headers: {
                              'Content-Type': el.image.type
                            }
                          });

                        case 9:
                          return _context.abrupt('return', {
                            elType: 'img',
                            image: '' + url + elUploadConfig.data.Key,
                            content: el.caption
                          });

                        case 10:
                        case 'end':
                          return _context.stop();
                      }
                    }
                  }, _callee, _this2);
                }));

                return function (_x) {
                  return _ref2.apply(this, arguments);
                };
              }());

              Promise.all(customs).then(function () {
                var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(results) {
                  var today, date, minutes, seconds, time, dateTime, objectForDatabase, uploadResponse;
                  return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                          today = new Date();
                          date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                          minutes = today.getMinutes();
                          seconds = today.getSeconds();
                          time = today.getHours() + ':' + ('' + (minutes.length === 1 ? '0' : '') + minutes) + ':' + ('' + (seconds.length === 1 ? '0' : '') + minutes);
                          dateTime = date + ' ' + time;
                          objectForDatabase = {
                            date: dateTime,
                            title: _this.state.inputs.title,
                            subTitle: _this.state.inputs.subTitle,
                            content: _this.state.inputs.content,
                            img: '' + url + uploadConfig.data.Key,
                            customs: results,
                            likes: 0
                          };
                          _context2.next = 9;
                          return _axios2.default.post('/api/post/upload', objectForDatabase);

                        case 9:
                          uploadResponse = _context2.sent;

                          _this.props.toggleLoader();
                          if (uploadResponse.data.error) {
                            _this.setState({
                              errors: _extends({}, _this.state.errors, { button: 'Please try again' }),
                              error: true
                            });
                          } else {
                            _this.props.postActions.resetPosts();
                            _this.setState({ success: uploadResponse.data.id });
                          }

                        case 12:
                        case 'end':
                          return _context2.stop();
                      }
                    }
                  }, _callee2, _this2);
                }));

                return function (_x2) {
                  return _ref3.apply(this, arguments);
                };
              }());
              _axios2.default.post('api/newsletter/send', { title: _this.state.inputs.title });

            case 10:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, _this2);
    }));

    _this.resetForm = function () {
      _this.setState(_extends({}, initialState, {
        inputs: {
          content: '',
          title: '',
          subTitle: ''
        }
      }));
    };

    _this.checkForErrors = function () {
      var customElements = _this.state.customElements;
      var inputs = _this.state.inputs;

      var error = !inputs.title || !inputs.content || !_this.state.mainPic;
      customElements.forEach(function (element, i) {
        if (element.type === 'image') {
          if (!element.image) {
            customElements[i].error = 'Please choose image';
            error = true;
          }
        } else {
          if (element.comtent = '') {
            customElements[i].error = "You don't want this to be empty ;)";
            error = true;
          }
        }
      });

      var errors = {
        title: inputs.title ? '' : 'Please add title to your post',
        content: inputs.content ? '' : 'That should not be empty ;p'
      };
      var mainPicError = _this.state.mainPic ? '' : 'You need main picture for your post!';
      _this.setState({ customElements: customElements, error: error, errors: errors, mainPicError: mainPicError });
    };

    _this.editElement = function (id, property) {
      return function (e) {
        var customElements = _this.state.customElements;

        var index = customElements.findIndex(function (el) {
          return el.id === id;
        });
        customElements[index][property] = e.target.value;
        customElements[index].error = '';

        _this.setState({ customElements: customElements, error: false });
      };
    };

    _this.deleteElement = function (index) {
      return function () {
        var customElements = _this.state.customElements;

        _this.setState({
          customElements: [].concat(_toConsumableArray(customElements.slice(0, index)), _toConsumableArray(customElements.slice(index + 1))),
          error: false
        });
      };
    };

    _this.elementOnDrop = function (id) {
      return function (img) {
        var customElements = _this.state.customElements;

        var index = customElements.findIndex(function (el) {
          return el.id === id;
        });
        customElements[index].image = img;
        customElements[index].error = '';

        _this.setState({ customElements: customElements, error: false });
      };
    };

    _this.state = initialState;
    return _this;
  }

  _createClass(AddPost, [{
    key: 'render',
    value: function render() {
      var _this3 = this;

      var hasError = function hasError(element) {
        return element.type === 'image' && !element.image || element.type === 'paragraph' && element.content === '';
      };
      var inputs = this.state.inputs;
      var errors = this.state.errors;
      var customElementsData = this.state.customElements;
      var isButtonDisabled = !inputs.title || !inputs.content || !this.state.mainPic;
      if (!isButtonDisabled) {
        isButtonDisabled = customElementsData.some(hasError);
      }
      var customElements = this.state.customElements.length === 0 ? [] : this.state.customElements.map(function (element, i) {
        var formElement = void 0;
        if (element.type === 'image') {
          formElement = _react2.default.createElement(
            _ComponentWrapper2.default,
            {
              title: 'Add picture',
              key: element.id,
              onDelete: _this3.deleteElement(i)
            },
            _react2.default.createElement(_Dropzone2.default, {
              isImage: true,
              'class': '--custom-pic',
              onDrop: _this3.elementOnDrop(element.id),
              caption: element.caption,
              changeCaption: _this3.editElement(element.id, 'caption'),
              error: element.error
            })
          );
        } else {
          formElement = _react2.default.createElement(
            _ComponentWrapper2.default,
            {
              title: 'Add new paragraph',
              key: element.id,
              onDelete: _this3.deleteElement(i)
            },
            _react2.default.createElement(_Input2.default, {
              'class': 'title-input',
              subClass: 'input--title',
              type: 'text',
              onChange: _this3.editElement(element.id, 'title'),
              placeholder: 'Maybe some title for your paragraph?',
              value: element.title
            }),
            _react2.default.createElement(_Input2.default, {
              'class': 'content-input',
              subClass: 'input--content textarea smaller',
              type: 'textarea',
              placeholder: 'Add some content to your post',
              onChange: _this3.editElement(element.id, 'content'),
              value: element.content,
              error: element.error
            })
          );
        }
        return formElement;
      });
      var elements = [_react2.default.createElement(
        _ComponentWrapper2.default,
        { title: 'Title', key: 'title' },
        _react2.default.createElement(_Input2.default, {
          'class': 'title-input',
          subClass: 'input--title',
          type: 'text',
          onChange: this.handleChange('title'),
          placeholder: 'Type in title for your post',
          value: this.state.inputs.title,
          error: errors.title
        }),
        _react2.default.createElement(_Input2.default, {
          'class': 'title-input',
          subClass: 'input--title',
          type: 'text',
          onChange: this.handleChange('subTitle'),
          placeholder: 'How about a subtitle?',
          value: this.state.inputs.subTitle
        })
      ), _react2.default.createElement(
        _ComponentWrapper2.default,
        { title: 'Main picture', key: 'main-pic' },
        _react2.default.createElement(_Dropzone2.default, {
          isImage: this.state.mainPic,
          'class': '__main-pic',
          onDrop: this.onDrop,
          error: this.state.mainPicError
        })
      ), _react2.default.createElement(
        _ComponentWrapper2.default,
        { title: 'Main content', key: 'content' },
        _react2.default.createElement(_Input2.default, {
          'class': 'content-input',
          subClass: 'input--content textarea',
          type: 'textarea',
          placeholder: 'Add some content to your post',
          onChange: this.handleChange('content'),
          value: this.state.inputs.content,
          error: errors.content
        })
      )].concat(_toConsumableArray(customElements), [_react2.default.createElement(
        _ComponentWrapper2.default,
        { title: 'Are you finished?', key: 'submit' },
        _react2.default.createElement(
          'div',
          { className: 'button-wrapper' },
          _react2.default.createElement(
            'button',
            {
              className: (0, _classnames2.default)('btn-send', { active: !isButtonDisabled }, { error: this.state.error }),
              onClick: isButtonDisabled ? this.checkForErrors : this.onSubmit
            },
            this.state.error && errors.button ? 'Please try again' : 'Submit'
          )
        )
      )]);
      return _react2.default.createElement(
        'section',
        { className: 'post-form' },
        this.state.success !== '' && _react2.default.createElement(_Success2.default, {
          repeat: this.resetForm,
          mainPage: function mainPage() {
            _this3.props.history.push('/');
          }
        }),
        _react2.default.createElement(
          _reactHelmet.Helmet,
          null,
          _react2.default.createElement(
            'title',
            null,
            'Add new post'
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'section-title' },
          _react2.default.createElement(
            'h1',
            null,
            'Add new post to your blog'
          )
        ),
        _react2.default.createElement(
          'ul',
          { className: 'form-elements' },
          elements
        ),
        _react2.default.createElement(
          'div',
          { className: 'post-form__custom-elements' },
          _react2.default.createElement(
            'div',
            { className: 'element' },
            _react2.default.createElement('div', { className: 'clickable', onClick: this.addNewImage }),
            _react2.default.createElement('a', { href: '#', className: 'fa fa-camera-retro' })
          ),
          _react2.default.createElement(
            'div',
            { className: 'element' },
            _react2.default.createElement('div', { className: 'clickable', onClick: this.addNewParagraph }),
            _react2.default.createElement('a', { href: '#', className: 'fa fa-pencil-square-o' })
          )
        )
      );
    }
  }]);

  return AddPost;
}(_react.Component);

AddPost.propTypes = {
  toggleLoader: _propTypes2.default.func,
  history: _propTypes2.default.object,
  postActions: _propTypes2.default.object
};

function mapDispatchToProps(dispatch) {
  return {
    postActions: (0, _redux.bindActionCreators)(postActions, dispatch)
  };
}

exports.default = {
  component: (0, _reactRouterDom.withRouter)((0, _reactRedux.connect)(null, mapDispatchToProps)((0, _requireAdmin2.default)(AddPost)))
};

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ComponentWrapper = function ComponentWrapper(props) {
  return _react2.default.createElement(
    'li',
    { className: 'component-wrapper' },
    props.onDelete && _react2.default.createElement(
      'div',
      { className: 'wrapper-close' },
      _react2.default.createElement('a', { href: '#', className: 'fa fa-times-circle', onClick: props.onDelete })
    ),
    _react2.default.createElement(
      'h3',
      null,
      props.title
    ),
    props.children
  );
};

ComponentWrapper.propTypes = {
  onDelete: _propTypes2.default.func,
  title: _propTypes2.default.string,
  children: _propTypes2.default.node
  // auth: PropTypes.object,
  // navOpen: PropTypes.bool
};

exports.default = ComponentWrapper;

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _fadeTransition = __webpack_require__(11);

var _fadeTransition2 = _interopRequireDefault(_fadeTransition);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Success = function (_Component) {
  _inherits(Success, _Component);

  function Success() {
    _classCallCheck(this, Success);

    return _possibleConstructorReturn(this, (Success.__proto__ || Object.getPrototypeOf(Success)).apply(this, arguments));
  }

  _createClass(Success, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'success-screen' },
        _react2.default.createElement(
          'h3',
          { className: 'title' },
          'Great! Now where do you want to go?'
        ),
        _react2.default.createElement(
          'div',
          { className: 'button-wrapper' },
          _react2.default.createElement(
            'button',
            { className: 'btn-send active', onClick: this.props.repeat },
            'Add another post'
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'button-wrapper' },
          _react2.default.createElement(
            'button',
            { className: 'btn-send active', onClick: this.props.mainPage },
            'Go to main page'
          )
        )
      );
    }
  }]);

  return Success;
}(_react.Component);

Success.propTypes = {
  repeat: _propTypes2.default.func,
  newPost: _propTypes2.default.func,
  mainPage: _propTypes2.default.func
};

exports.default = (0, _fadeTransition2.default)(Success);

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactRedux = __webpack_require__(4);

var _reactRouterDom = __webpack_require__(7);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

exports.default = function (ChildComponent) {
  var RequireAdmin = function (_Component) {
    _inherits(RequireAdmin, _Component);

    function RequireAdmin() {
      _classCallCheck(this, RequireAdmin);

      return _possibleConstructorReturn(this, (RequireAdmin.__proto__ || Object.getPrototypeOf(RequireAdmin)).apply(this, arguments));
    }

    _createClass(RequireAdmin, [{
      key: 'render',
      value: function render() {
        var isSignedIn = !!this.props.auth.authenticated && !!this.props.auth.authenticated.user;
        var isAdmin = isSignedIn && this.props.auth.authenticated.isAdmin;
        switch (isAdmin) {
          case true:
            return _react2.default.createElement(ChildComponent, this.props);
          default:
            return _react2.default.createElement(_reactRouterDom.Redirect, { to: '/' });
        }
      }
    }]);

    return RequireAdmin;
  }(_react.Component);

  function mapStateToProps(state) {
    return { auth: state.auth };
  }

  return (0, _reactRedux.connect)(mapStateToProps)(RequireAdmin);
};

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactDropzone = __webpack_require__(56);

var _reactDropzone2 = _interopRequireDefault(_reactDropzone);

var _classnames = __webpack_require__(5);

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DropzoneComponent = function (_React$Component) {
  _inherits(DropzoneComponent, _React$Component);

  function DropzoneComponent() {
    _classCallCheck(this, DropzoneComponent);

    var _this = _possibleConstructorReturn(this, (DropzoneComponent.__proto__ || Object.getPrototypeOf(DropzoneComponent)).call(this));

    _this.onDrop = function (acceptedFiles) {
      var reader = new FileReader();
      reader.onload = function (e) {
        _this.setState({
          imagePreview: e.target.result
        });
      };
      reader.readAsDataURL(acceptedFiles[0]);
      _this.props.onDrop(acceptedFiles[0]);
    };

    _this.state = { imagePreview: null };
    return _this;
  }

  _createClass(DropzoneComponent, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'post-form dropzone dropzone' + this.props.class },
        _react2.default.createElement(
          _reactDropzone2.default,
          { onDrop: this.onDrop, accept: 'image/*' },
          function (_ref) {
            var getRootProps = _ref.getRootProps,
                getInputProps = _ref.getInputProps,
                isDragActive = _ref.isDragActive;

            return _react2.default.createElement(
              'div',
              _extends({}, getRootProps(), {
                className: (0, _classnames2.default)('dropzone', {
                  'dropzone--isActive': isDragActive,
                  'transparent-dropzone': _this2.state.imagePreview && _this2.props.isImage,
                  error: _this2.props.error
                })
              }),
              _react2.default.createElement('input', getInputProps()),
              _this2.props.error ? _react2.default.createElement(
                'p',
                null,
                'Please choose some image'
              ) : isDragActive ? _react2.default.createElement(
                'p',
                null,
                'Drop file here...'
              ) : _react2.default.createElement(
                'p',
                null,
                'Do you need title picture? Drop it here or simply click and select it'
              )
            );
          }
        ),
        _react2.default.createElement('img', {
          id: 'target',
          src: this.props.isImage ? this.state.imagePreview : null
        }),
        this.props.changeCaption && _react2.default.createElement(
          'div',
          { className: 'input-holder img-caption' },
          _react2.default.createElement('input', {
            className: 'input input--caption',
            type: 'text',
            placeholder: ' ',
            onChange: this.props.changeCaption,
            value: this.props.caption,
            ref: function ref(input) {
              _this2.titleInput = input;
            }
          }),
          _react2.default.createElement(
            'div',
            {
              className: 'placeholder placeholder--caption',
              onClick: function onClick() {
                _this2.titleInput.focus();
              }
            },
            'Maybe you want to add some caption for the image?'
          )
        )
      );
    }
  }]);

  return DropzoneComponent;
}(_react2.default.Component);

exports.default = DropzoneComponent;

/***/ }),
/* 56 */
/***/ (function(module, exports) {

module.exports = require("react-dropzone");

/***/ }),
/* 57 */
/***/ (function(module, exports) {

module.exports = require("serialize-javascript");

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = __webpack_require__(6);

var _reduxThunk = __webpack_require__(59);

var _reduxThunk2 = _interopRequireDefault(_reduxThunk);

var _reducers = __webpack_require__(60);

var _reducers2 = _interopRequireDefault(_reducers);

var _axios = __webpack_require__(9);

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (req) {
  var axiosInstance = _axios2.default.create({
    headers: { cookie: req.get('cookie') || '' }
  });
  var store = (0, _redux.createStore)(_reducers2.default, {
    auth: {
      authenticated: req.session ? req.session.user || null : null,
      modalOpen: false,
      errorLogin: null,
      errorPassword: null
    }
  }, (0, _redux.applyMiddleware)(_reduxThunk2.default.withExtraArgument(axiosInstance)));

  return store;
};

/***/ }),
/* 59 */
/***/ (function(module, exports) {

module.exports = require("redux-thunk");

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = __webpack_require__(6);

var _auth = __webpack_require__(61);

var _post = __webpack_require__(62);

var rootReducer = (0, _redux.combineReducers)({
  auth: _auth.authReducer,
  post: _post.post
});

exports.default = rootReducer;

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.authReducer = authReducer;

var _authActionTypes = __webpack_require__(23);

var INITIAL_STATE = {
  authenticated: null,
  modalOpen: false,
  errorLogin: null,
  errorPassword: null
};

function authReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
  var action = arguments[1];

  switch (action.type) {
    case _authActionTypes.AUTH_USER:
      {
        return _extends({}, state, {
          authenticated: action.payload,
          modalOpen: false,
          errorLogin: null,
          errorPassword: null
        });
      }
    case _authActionTypes.TOGGLE_MODAL:
      {
        return _extends({}, state, {
          modalOpen: !state.modalOpen,
          errorLogin: null,
          errorPassword: null
        });
      }
    case _authActionTypes.ERROR_LOGIN:
      {
        return _extends({}, state, {
          errorLogin: action.payload
        });
      }
    case _authActionTypes.ERROR_PASSWORD:
      {
        return _extends({}, state, {
          errorPassword: action.payload
        });
      }
    case _authActionTypes.RESET_ERRORS:
      {
        return _extends({}, state, {
          errorPassword: null,
          errorLogin: null
        });
      }

    default:
      {
        return state;
      }
  }
}

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.post = post;

var _postActionTypes = __webpack_require__(24);

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var INITIAL_STATE = {
  posts: [],
  selectedPost: null,
  count: 0
};

function post() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
  var action = arguments[1];

  switch (action.type) {
    case _postActionTypes.GET_DUMMY_POSTS:
      {
        var dummyPosts = [];
        for (var i = 0; i < action.payload.length; i++) {
          dummyPosts.push({ _id: i, loader: true, dummy: true });
        }
        return _extends({}, state, {
          posts: [].concat(_toConsumableArray(state.posts), dummyPosts)
        });
      }
    case _postActionTypes.GET_POST_COUNT:
      {
        return _extends({}, state, {
          count: action.payload.count
        });
      }
    case _postActionTypes.RESET_POST:
      {
        return _extends({}, state, {
          selectedPost: null
        });
      }
    case _postActionTypes.ADD_COMMENT:
      {
        return _extends({}, state, {
          selectedPost: _extends({}, state.selectedPost, {
            comments: [action.payload.comment].concat(_toConsumableArray(state.selectedPost.comments))
          })
        });
      }
    case _postActionTypes.GET_MAIN_POST:
      {
        return _extends({}, state, {
          selectedPost: action.payload.post
        });
      }
    case _postActionTypes.RESET_POSTS:
      {
        return _extends({}, state, {
          posts: [],
          selectedPost: null
        });
      }
    case _postActionTypes.REPLACE_DUMMY_POSTS:
      {
        var indexToDeleteFrom = state.posts.findIndex(function (el) {
          return el.dummy;
        });
        return _extends({}, state, {
          posts: [].concat(_toConsumableArray(state.posts.slice(0, indexToDeleteFrom)), _toConsumableArray(action.payload.posts))
        });
      }
    default:
      {
        return state;
      }
  }
}

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = __webpack_require__(3);

var _express2 = _interopRequireDefault(_express);

var _authentication = __webpack_require__(64);

var _passport = __webpack_require__(67);

var _passport2 = __webpack_require__(70);

var _passport3 = _interopRequireDefault(_passport2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_passport3.default.use(_passport.jwtLogin);
_passport3.default.use(_passport.localLogin);
//if user is auth'd do not create session for him
// const requireAuth = passport.authenticate('jwt', { session: false });
var requireSignin = _passport3.default.authenticate('local', { session: false });

var authRouter = _express2.default.Router();
authRouter.post('/session/get', function (req, res) {
  if (typeof req.session.user !== 'undefined' && req.session.user.token) {
    res.json(req.session.user);
  } else {
    res.json({ msg: 'no signed user' });
  }
});
authRouter.post('/session/delete', function (req, res) {
  req.session.user = {};
  req.session.save(function (err) {
    if (err) {
      throw err;
    }
    res.json(req.session.user);
  });
});
// authRouter.get('/', requireAuth, function(req, res) {
//   // res.send({ message: 'Super secret code is ABC123' });
//   // if(typeof req.session.user !== 'undefined') {
//   //   res.json(req.session.user);
//   // }
// });
authRouter.post('/signin', requireSignin, _authentication.signin);
authRouter.post('/signup', _authentication.signup);
exports.default = authRouter;

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.signup = exports.signin = undefined;

var _jwtSimple = __webpack_require__(65);

var _jwtSimple2 = _interopRequireDefault(_jwtSimple);

var _config = __webpack_require__(8);

var _user = __webpack_require__(26);

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var tokenForUser = function tokenForUser(user) {
  var timestamp = new Date().getTime();
  return _jwtSimple2.default.encode({ sub: user.id, iat: timestamp }, _config.secretString);
};

var signin = function signin(req, res) {
  //user had his em and pass auth'd so now his going to receive token
  //passport assigns user (inside comparePassword) to req.user
  if (req.user.error) {
    res.send(req.user);
  } else {
    var token = tokenForUser(req.user);
    req.session.user = {
      user: req.user.email,
      isAdmin: req.user.isAdmin,
      token: token,
      id: req.user._id
    };
    req.session.save(function (err) {
      if (err) {
        throw err;
      }
      res.json(req.session.user);
    });
  }
};

var signup = function signup(req, res, next) {
  var _req$body = req.body,
      email = _req$body.email,
      password = _req$body.password,
      passwordCheck = _req$body.passwordCheck;

  if (!email || !password || !passwordCheck) {
    return res.send({ errorLogin: 'Provide email and both passwords' });
  }
  if (passwordCheck !== password) {
    return res.send({ errorPassword: 'Both passwords must be the same' });
  }
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var isEmailValid = re.test(String(email).toLowerCase());
  if (!isEmailValid) {
    return res.send({ errorLogin: "This email doesn't look right.... " });
  }

  _user2.default.findOne({ email: email }, function (err, existingUser) {
    if (err) {
      return next(err);
    }

    if (existingUser) {
      return res.status(422).send({ errorLogin: 'Email is already in use' });
    }

    var user = new _user2.default({ email: email, password: password });
    user.save(function (err) {
      if (err) {
        return next(err);
      }
      var token = tokenForUser(user);
      req.session.user = {
        user: user.email,
        isAdmin: false,
        token: token
      };
      req.session.save(function (err) {
        if (err) {
          throw err;
        }
        res.json(req.session.user);
      });
    });
  });
};

exports.signin = signin;
exports.signup = signup;

/***/ }),
/* 65 */
/***/ (function(module, exports) {

module.exports = require("jwt-simple");

/***/ }),
/* 66 */
/***/ (function(module, exports) {

module.exports = require("bcrypt-nodejs");

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.localLogin = exports.jwtLogin = undefined;

var _user = __webpack_require__(26);

var _user2 = _interopRequireDefault(_user);

var _config = __webpack_require__(8);

var _passportJwt = __webpack_require__(68);

var _passportLocal = __webpack_require__(69);

var _passportLocal2 = _interopRequireDefault(_passportLocal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var localOptions = { usernameField: 'email' };
var localLogin = new _passportLocal2.default(localOptions, function (email, password, done) {
  // Verify this email and password, call done with the user
  // if it is the correct email and password
  // otherwise, call done with false
  _user2.default.findOne({ email: email }, function (err, user) {
    if (err) {
      return done(err);
    }
    if (!user) {
      return done(null, { error: 'You typed wrong username or password' });
    }

    user.comparePassword(password, function (err, isMatch) {
      if (err) {
        return done(err);
      }
      if (!isMatch) {
        return done(null, { error: 'You typed wrong username or password' });
      }

      return done(null, user);
    });
  });
});

var jwtOptions = {
  jwtFromRequest: _passportJwt.ExtractJwt.fromHeader('authorization'),
  secretOrKey: _config.secretString
};
//payload - user id and time stamp
var jwtLogin = new _passportJwt.Strategy(jwtOptions, function (payload, done) {
  _user2.default.findById(payload.sub, function (err, user) {
    if (err) {
      return done(err, false);
    }
    if (user) {
      done(null, user);
    } else {
      done(null, false);
    }
  });
});

exports.jwtLogin = jwtLogin;
exports.localLogin = localLogin;

/***/ }),
/* 68 */
/***/ (function(module, exports) {

module.exports = require("passport-jwt");

/***/ }),
/* 69 */
/***/ (function(module, exports) {

module.exports = require("passport-local");

/***/ }),
/* 70 */
/***/ (function(module, exports) {

module.exports = require("passport");

/***/ })
/******/ ]);