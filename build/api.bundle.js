/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 71);
/******/ })
/************************************************************************/
/******/ ({

/***/ 13:
/***/ (function(module, exports) {

module.exports = require("babel-polyfill");

/***/ }),

/***/ 15:
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ 16:
/***/ (function(module, exports) {

module.exports = require("morgan");

/***/ }),

/***/ 19:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _connectMongo = __webpack_require__(20);

var _connectMongo2 = _interopRequireDefault(_connectMongo);

var _expressSession = __webpack_require__(21);

var _expressSession2 = _interopRequireDefault(_expressSession);

var _mongoose = __webpack_require__(2);

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MongoStore = (0, _connectMongo2.default)(_expressSession2.default);

var useSession = function useSession(app) {
  app.use((0, _expressSession2.default)({
    secret: 'mySecretString',
    saveUninitialized: false,
    resave: false,
    cookie: { maxAge: 1000 * 60 * 60 * 24 * 2 },
    store: new MongoStore({
      mongooseConnection: _mongoose2.default.connection,
      ttl: 2 * 24 * 60 * 60
    })
  }));
};

exports.default = useSession;

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ 20:
/***/ (function(module, exports) {

module.exports = require("connect-mongo");

/***/ }),

/***/ 21:
/***/ (function(module, exports) {

module.exports = require("express-session");

/***/ }),

/***/ 27:
/***/ (function(module, exports) {

module.exports = require("nodemailer");

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ 71:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(13);

var _express = __webpack_require__(3);

var _express2 = _interopRequireDefault(_express);

var _cookieParser = __webpack_require__(72);

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _bodyParser = __webpack_require__(15);

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _morgan = __webpack_require__(16);

var _morgan2 = _interopRequireDefault(_morgan);

var _mongoose = __webpack_require__(2);

var _mongoose2 = _interopRequireDefault(_mongoose);

var _imageRoutes = __webpack_require__(73);

var _imageRoutes2 = _interopRequireDefault(_imageRoutes);

var _postRoutes = __webpack_require__(76);

var _postRoutes2 = _interopRequireDefault(_postRoutes);

var _contactRoutes = __webpack_require__(79);

var _contactRoutes2 = _interopRequireDefault(_contactRoutes);

var _newsletterRoutes = __webpack_require__(80);

var _newsletterRoutes2 = _interopRequireDefault(_newsletterRoutes);

var _useSession = __webpack_require__(19);

var _useSession2 = _interopRequireDefault(_useSession);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();
app.use((0, _morgan2.default)('dev'));
app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: false }));
app.use((0, _cookieParser2.default)());
_mongoose2.default.connect('mongodb://kreha:cms123@ds145562.mlab.com:45562/cms');

(0, _useSession2.default)(app);

app.use('/img', _imageRoutes2.default);
app.use('/post', _postRoutes2.default);
app.use('/contact', _contactRoutes2.default);
app.use('/newsletter', _newsletterRoutes2.default);

app.listen(3001, function (err) {
  if (err) {
    console.log(err);
  }
  console.log('api server on port 3001');
});

/***/ }),

/***/ 72:
/***/ (function(module, exports) {

module.exports = require("cookie-parser");

/***/ }),

/***/ 73:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = __webpack_require__(3);

var _express2 = _interopRequireDefault(_express);

var _awsSdk = __webpack_require__(74);

var _awsSdk2 = _interopRequireDefault(_awsSdk);

var _v = __webpack_require__(75);

var _v2 = _interopRequireDefault(_v);

var _config = __webpack_require__(8);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var s3 = new _awsSdk2.default.S3({
  signatureVersion: 'v4',
  accessKeyId: _config.accessKeyId,
  secretAccessKey: _config.secretAccessKey,
  region: 'eu-central-1'
});

var imageRouter = _express2.default.Router();

imageRouter.post('/upload', function (req, res) {
  var type = req.body.ContentType;
  var Key = req.session.user.id + '/' + (0, _v2.default)() + '.' + type;

  s3.getSignedUrl('putObject', {
    Bucket: 'cms-bucket-06',
    ContentType: type,
    Key: Key
  }, function (err, url) {
    return res.send({ Key: Key, url: url });
  });
});

exports.default = imageRouter;

/***/ }),

/***/ 74:
/***/ (function(module, exports) {

module.exports = require("aws-sdk");

/***/ }),

/***/ 75:
/***/ (function(module, exports) {

module.exports = require("uuid/v1");

/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = __webpack_require__(3);

var _express2 = _interopRequireDefault(_express);

var _post = __webpack_require__(77);

var _post2 = _interopRequireDefault(_post);

var _comment = __webpack_require__(78);

var _comment2 = _interopRequireDefault(_comment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var postRouter = _express2.default.Router();

postRouter.post('/upload', function (req, res) {
  var post = req.body;
  var newPost = new _post2.default(post);
  newPost.save(function (err) {
    if (err) {
      return res.send({ error: 'Something went wrong' });
    }
    res.json(newPost);
  });
});

postRouter.post('/add-comment', function (req, res) {
  var _req$body = req.body,
      comment = _req$body.comment,
      postId = _req$body.postId;

  var newComment = new _comment2.default(comment);
  newComment.save(function (err) {
    if (err) {
      return res.send({ error: 'Something went wrong' });
    }
    _post2.default.update({ _id: postId }, {
      $push: {
        comments: newComment._id
      }
    }, function (err) {
      if (err) {
        return res.send(err);
      }
      return res.json(newComment);
    });
  });
});

postRouter.post('/count', function (req, res) {
  _post2.default.count({}, function (err, count) {
    res.send({ count: count });
  });
});

postRouter.get('/get/:skip/:limit', function (req, res) {
  var _req$params = req.params,
      skip = _req$params.skip,
      limit = _req$params.limit;

  var query = _post2.default.find().skip(parseInt(skip)).limit(parseInt(limit)).sort('-date').select({ title: 1, img: 1, date: 1, likes: 1, subTitle: 1, dateValue: 1 });

  query.exec(function (err, docs) {
    if (err) {
      return res.send({ error: 'Something went wrong' });
    }
    res.json(docs);
  });
});

postRouter.post('/get', function (req, res) {
  var _id = req.body._id;
  _post2.default.findById(_id).populate('comments').exec(function (err, doc) {
    if (err) {
      return res.send({ error: 'Something went wrong' });
    }
    res.json(doc);
  });
});

exports.default = postRouter;

/***/ }),

/***/ 77:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = __webpack_require__(2);

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var postSchema = new Schema({
  date: String,
  title: String,
  subTitle: String,
  content: String,
  img: String,
  comments: [{ type: Schema.Types.ObjectId, ref: 'comment' }],
  likes: { type: Number, default: 0 },
  customs: [{
    elType: String,
    content: String,
    title: String,
    image: String
  }],
  dateValue: String
});

var modelClass = _mongoose2.default.model('post', postSchema);

exports.default = modelClass;

/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = __webpack_require__(2);

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var commentSchema = new Schema({
  date: String,
  content: String,
  author: String
});

var modelClass = _mongoose2.default.model('comment', commentSchema);

exports.default = modelClass;

/***/ }),

/***/ 79:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = __webpack_require__(3);

var _express2 = _interopRequireDefault(_express);

var _nodemailer = __webpack_require__(27);

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _config = __webpack_require__(8);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var contactRouter = _express2.default.Router();

contactRouter.post('*', function (req, res) {
  var transporter = _nodemailer2.default.createTransport({
    service: 'gmail',
    auth: {
      user: _config.mailLogin,
      pass: _config.mailPassword
    },
    tls: {
      rejectUnauthorized: false
    }
  });

  var mailOptions = {
    from: 'mailLogin',
    to: 'cmscontact1234@gmail.com',
    subject: req.body.subject,
    text: req.body.message,
    replyTo: req.body.contact
  };

  transporter.sendMail(mailOptions, function (err, info) {
    if (err) res.send({ error: 'Something went wrong...... please try again :(' });else res.send(info);
  });
});

exports.default = contactRouter;

/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var secretString = exports.secretString = 'gds_%*ghj654657if#$kj%m@cnb#r6hd#seg5%6esbsgr5^6tgdtb';
var accessKeyId = exports.accessKeyId = 'AKIAJQGCQU7APS4VZMHA';
var secretAccessKey = exports.secretAccessKey = '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';
var mailLogin = exports.mailLogin = 'cmsserver12@gmail.com';
var mailPassword = exports.mailPassword = 'cms1234-';

/***/ }),

/***/ 80:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = __webpack_require__(3);

var _express2 = _interopRequireDefault(_express);

var _nodemailer = __webpack_require__(27);

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _newsletter = __webpack_require__(81);

var _newsletter2 = _interopRequireDefault(_newsletter);

var _config = __webpack_require__(8);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var newsletterRouter = _express2.default.Router();
var id = '5c4a2ebbf42acb0a643ff007';

newsletterRouter.post('/create', function (req, res) {
  var newsletter = new _newsletter2.default({ emails: [] });
  newsletter.save(function (err) {
    if (err) {
      return res.send({ error: 'Something went wrong' });
    }
    return res.send({ ok: 'created' });
  });
});

newsletterRouter.post('/add', function (req, res) {
  var email = req.body.email;
  _newsletter2.default.update({ _id: id }, {
    $push: {
      emails: email
    }
  }, function (err) {
    if (err) {
      return res.send(err);
    }
    return res.json({ ok: 'ok' });
  });
});

newsletterRouter.post('/send', function (req, res) {
  var title = req.body.title;
  _newsletter2.default.findById(id, function (err, list) {
    if (err) {
      return res.send({ error: 'Something went wrong' });
    }
    var transporter = _nodemailer2.default.createTransport({
      service: 'gmail',
      auth: {
        user: _config.mailLogin,
        pass: _config.mailPassword
      },
      tls: {
        rejectUnauthorized: false
      }
    });
    list.emails.forEach(function (email) {
      var mailOptions = {
        from: 'PostBlog',
        to: email,
        subject: 'New post on my blog!',
        text: 'Hi! I just added new post: ' + title + '. Come check it out!'
      };
      transporter.sendMail(mailOptions, function (err) {
        if (err) res.send({ error: 'Something went wrong...... please try again :(' });
      });
    });
  });
  return res.send({ ok: 'ok' });
});

exports.default = newsletterRouter;

/***/ }),

/***/ 81:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = __webpack_require__(2);

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var newsletterSchema = new Schema({
  emails: []
});

var modelClass = _mongoose2.default.model('newsletter', newsletterSchema);

exports.default = modelClass;

/***/ })

/******/ });