export const GET_DUMMY_POSTS = 'GET_DUMMY_POSTS';
export const GET_POST_COUNT = 'GET_POST_COUNT';
export const REPLACE_DUMMY_POSTS = 'REPLACE_DUMMY_POSTS';
export const UNSHIFT_POST = 'UNSHIFT_POST';
export const RESET_POSTS = 'RESET_POSTS';
export const GET_MAIN_POST = 'GET_MAIN_POST';
export const RESET_POST = 'RESET_POST';
export const ADD_COMMENT = 'ADD_COMMENT';
