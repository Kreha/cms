export const AUTH_USER = 'AUTH_USER';
export const TOGGLE_MODAL = 'TOGGLE_MODAL';
export const ERROR_LOGIN = 'ERROR_LOGIN';
export const ERROR_PASSWORD = 'ERROR_PASSWORD';
export const RESET_ERRORS = 'RESET_ERRORS';
