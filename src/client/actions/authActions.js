import axios from 'axios';
import {
  AUTH_USER,
  TOGGLE_MODAL,
  ERROR_LOGIN,
  ERROR_PASSWORD,
  RESET_ERRORS
} from './../constants/authActionTypes';

export const signUp = (signData, callback) => async dispatch => {
  try {
    const response = await axios.post('/auth/signup', signData);
    if (response.data.errorLogin) {
      dispatch({ type: ERROR_LOGIN, payload: response.data.errorLogin });
    } else if (response.data.errorPassword) {
      dispatch({ type: ERROR_PASSWORD, payload: response.data.errorPassword });
    } else {
      dispatch({ type: AUTH_USER, payload: response.data });
    }
    if (callback) {
      callback();
    }
  } catch (e) {
    dispatch({ type: ERROR_LOGIN, payload: 'Some unexpected error happened' });
  }
};

export const signIn = (signData, callback) => async dispatch => {
  try {
    const response = await axios.post('/auth/signin', signData);
    if (!response.data.error) {
      dispatch({ type: AUTH_USER, payload: response.data });
    } else {
      dispatch({ type: ERROR_LOGIN, payload: response.data.error });
    }
    if (callback) {
      callback();
    }
  } catch (e) {
    dispatch({ type: ERROR_LOGIN, payload: 'Some unexpected error happened' });
  }
};

export const signOut = () => {
  axios.post('/auth/session/delete');
  return {
    type: AUTH_USER,
    payload: null
  };
};

export const resetErrors = () => {
  return { type: RESET_ERRORS };
};

export function toggleModal() {
  return function(dispatch) {
    dispatch({
      type: TOGGLE_MODAL
    });
  };
}

export const getUser = () => async dispatch => {
  try {
    const response = await axios.post('/auth/session/get');
    dispatch({ type: AUTH_USER, payload: response.data });
  } catch (e) {
    dispatch({ type: ERROR_LOGIN, payload: null });
  }
};

export const signOutUser = () => async dispatch => {
  try {
    axios.post('/auth/session/delete');
  } catch (e) {
    dispatch({ type: ERROR_LOGIN, payload: 'error' });
  }
};
