import axios from 'axios';
import {
  GET_DUMMY_POSTS,
  GET_POST_COUNT,
  REPLACE_DUMMY_POSTS,
  RESET_POSTS,
  GET_MAIN_POST,
  RESET_POST,
  ADD_COMMENT
} from './../constants/postActionTypes';

export function createDummyPosts(count) {
  return function(dispatch) {
    dispatch({
      type: GET_DUMMY_POSTS,
      payload: {
        length: count
      }
    });
  };
}
export function resetPosts() {
  return function(dispatch) {
    dispatch({
      type: RESET_POSTS
    });
  };
}
export function resetPost() {
  return function(dispatch) {
    dispatch({
      type: RESET_POST
    });
  };
}

export function getPosts(skip) {
  return function(dispatch) {
    axios.get(`/api/post/get/${skip}/9`).then(response => {
      dispatch({
        type: REPLACE_DUMMY_POSTS,
        payload: {
          posts: response.data
        }
      });
    });
  };
}
export function addComment(data) {
  return function(dispatch) {
    axios.post('api/post/add-comment', data).then(response => {
      dispatch({
        type: ADD_COMMENT,
        payload: {
          comment: response.data
        }
      });
    });
  };
}

export function getPost(_id) {
  return function(dispatch) {
    axios.post('/api/post/get', { _id }).then(response => {
      dispatch({
        type: GET_MAIN_POST,
        payload: {
          post: response.data
        }
      });
    });
  };
}

export function getPostsCount() {
  return function(dispatch) {
    axios.post('/api/post/count', {}).then(response => {
      dispatch({
        type: GET_POST_COUNT,
        payload: {
          count: response.data.count
        }
      });
    });
  };
}
