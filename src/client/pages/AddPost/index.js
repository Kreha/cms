import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { withRouter } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import axios from 'axios';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import * as postActions from './../../actions/postActions';
import ComponentWrapper from './ComponentWrapper';
import Success from './Success';
import requireAdmin from '../../components/hocs/requireAdmin';
import Dropzone from './../../components/Dropzone';
import Input from './../../components/Input';

const url = 'https://s3.eu-central-1.amazonaws.com/cms-bucket-06/';

const initialState = {
  success: '',
  mainPic: null,
  mainPicError: '',
  inputs: {
    content: '',
    title: '',
    subTitle: ''
  },
  customElements: [],
  nextCustomId: 1,
  errors: {
    content: '',
    title: '',
    image: '',
    button: ''
  },
  error: false
};

class AddPost extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  onDrop = acceptedFile => {
    this.setState({
      mainPic: acceptedFile,
      mainPicError: '',
      error: false,
      errors: {
        ...this.state.errors,
        button: ''
      }
    });
  };

  handleChange = propertyName => e => {
    const { inputs } = this.state;
    const { errors } = this.state;

    inputs[propertyName] = e.target.value;
    errors[propertyName] = '';

    this.setState({ inputs, errors: { ...errors, button: '' }, error: false });
  };

  addNewParagraph = () => {
    this.setState({
      success: '',
      nextCustomId: this.state.nextCustomId + 1,
      customElements: [
        ...this.state.customElements,
        {
          id: this.state.nextCustomId,
          type: 'paragraph',
          content: '',
          title: '',
          error: ''
        }
      ],
      errors: {
        ...this.state.errors,
        button: ''
      }
    });
  };

  addNewImage = () => {
    this.setState({
      nextCustomId: this.state.nextCustomId + 1,
      customElements: [
        ...this.state.customElements,
        {
          id: this.state.nextCustomId,
          type: 'image',
          image: null,
          caption: '',
          error: ''
        }
      ]
    });
  };

  onSubmit = async () => {
    this.props.toggleLoader();
    const file = this.state.mainPic;
    const uploadConfig = await axios.post('/api/img/upload', {
      ContentType: file.type
    });
    await axios.put(uploadConfig.data.url, file, {
      headers: {
        'Content-Type': file.type
      }
    });

    const customs = this.state.customElements.map(async el => {
      if (el.type === 'paragraph') {
        return {
          elType: 'p',
          content: el.content,
          title: el.title
        };
      } else {
        const elUploadConfig = await axios.post('/api/img/upload', {
          ContentType: el.image.type
        });
        await axios.put(elUploadConfig.data.url, el.image, {
          headers: {
            'Content-Type': el.image.type
          }
        });
        return {
          elType: 'img',
          image: `${url}${elUploadConfig.data.Key}`,
          content: el.caption
        };
      }
    });
    Promise.all(customs).then(async results => {
      const today = new Date();
      const date =
        today.getFullYear() +
        '-' +
        (today.getMonth() + 1) +
        '-' +
        today.getDate();
      const minutes = today.getMinutes();
      const seconds = today.getSeconds();
      const time =
        today.getHours() +
        ':' +
        `${minutes.length === 1 ? '0' : ''}${minutes}` +
        ':' +
        `${seconds.length === 1 ? '0' : ''}${minutes}`;
      const dateTime = date + ' ' + time;
      const objectForDatabase = {
        date: dateTime,
        title: this.state.inputs.title,
        subTitle: this.state.inputs.subTitle,
        content: this.state.inputs.content,
        img: `${url}${uploadConfig.data.Key}`,
        customs: results,
        likes: 0
      };
      const uploadResponse = await axios.post(
        '/api/post/upload',
        objectForDatabase
      );
      this.props.toggleLoader();
      if (uploadResponse.data.error) {
        this.setState({
          errors: { ...this.state.errors, button: 'Please try again' },
          error: true
        });
      } else {
        this.props.postActions.resetPosts();
        this.setState({ success: uploadResponse.data.id });
      }
    });
    axios.post('api/newsletter/send', { title: this.state.inputs.title });
  };

  resetForm = () => {
    this.setState({
      ...initialState,
      inputs: {
        content: '',
        title: '',
        subTitle: ''
      }
    });
  };

  checkForErrors = () => {
    const { customElements } = this.state;
    const { inputs } = this.state;
    let error = !inputs.title || !inputs.content || !this.state.mainPic;
    customElements.forEach((element, i) => {
      if (element.type === 'image') {
        if (!element.image) {
          customElements[i].error = 'Please choose image';
          error = true;
        }
      } else {
        if ((element.comtent = '')) {
          customElements[i].error = "You don't want this to be empty ;)";
          error = true;
        }
      }
    });

    const errors = {
      title: inputs.title ? '' : 'Please add title to your post',
      content: inputs.content ? '' : 'That should not be empty ;p'
    };
    const mainPicError = this.state.mainPic
      ? ''
      : 'You need main picture for your post!';
    this.setState({ customElements, error, errors, mainPicError });
  };

  editElement = (id, property) => e => {
    const { customElements } = this.state;
    const index = customElements.findIndex(el => el.id === id);
    customElements[index][property] = e.target.value;
    customElements[index].error = '';

    this.setState({ customElements, error: false });
  };
  deleteElement = index => () => {
    const { customElements } = this.state;
    this.setState({
      customElements: [
        ...customElements.slice(0, index),
        ...customElements.slice(index + 1)
      ],
      error: false
    });
  };

  elementOnDrop = id => img => {
    const { customElements } = this.state;
    const index = customElements.findIndex(el => el.id === id);
    customElements[index].image = img;
    customElements[index].error = '';

    this.setState({ customElements, error: false });
  };

  render() {
    const hasError = element =>
      (element.type === 'image' && !element.image) ||
      (element.type === 'paragraph' && element.content === '');
    const inputs = this.state.inputs;
    const errors = this.state.errors;
    const customElementsData = this.state.customElements;
    let isButtonDisabled =
      !inputs.title || !inputs.content || !this.state.mainPic;
    if (!isButtonDisabled) {
      isButtonDisabled = customElementsData.some(hasError);
    }
    const customElements =
      this.state.customElements.length === 0
        ? []
        : this.state.customElements.map((element, i) => {
            let formElement;
            if (element.type === 'image') {
              formElement = (
                <ComponentWrapper
                  title="Add picture"
                  key={element.id}
                  onDelete={this.deleteElement(i)}
                >
                  <Dropzone
                    isImage={true}
                    class="--custom-pic"
                    onDrop={this.elementOnDrop(element.id)}
                    caption={element.caption}
                    changeCaption={this.editElement(element.id, 'caption')}
                    error={element.error}
                  />
                </ComponentWrapper>
              );
            } else {
              formElement = (
                <ComponentWrapper
                  title="Add new paragraph"
                  key={element.id}
                  onDelete={this.deleteElement(i)}
                >
                  <Input
                    class="title-input"
                    subClass="input--title"
                    type="text"
                    onChange={this.editElement(element.id, 'title')}
                    placeholder="Maybe some title for your paragraph?"
                    value={element.title}
                  />
                  <Input
                    class="content-input"
                    subClass="input--content textarea smaller"
                    type="textarea"
                    placeholder="Add some content to your post"
                    onChange={this.editElement(element.id, 'content')}
                    value={element.content}
                    error={element.error}
                  />
                </ComponentWrapper>
              );
            }
            return formElement;
          });
    const elements = [
      <ComponentWrapper title="Title" key="title">
        <Input
          class="title-input"
          subClass="input--title"
          type="text"
          onChange={this.handleChange('title')}
          placeholder="Type in title for your post"
          value={this.state.inputs.title}
          error={errors.title}
        />
        <Input
          class="title-input"
          subClass="input--title"
          type="text"
          onChange={this.handleChange('subTitle')}
          placeholder="How about a subtitle?"
          value={this.state.inputs.subTitle}
        />
      </ComponentWrapper>,
      <ComponentWrapper title="Main picture" key="main-pic">
        <Dropzone
          isImage={this.state.mainPic}
          class="__main-pic"
          onDrop={this.onDrop}
          error={this.state.mainPicError}
        />
      </ComponentWrapper>,
      <ComponentWrapper title="Main content" key="content">
        <Input
          class="content-input"
          subClass="input--content textarea"
          type="textarea"
          placeholder="Add some content to your post"
          onChange={this.handleChange('content')}
          value={this.state.inputs.content}
          error={errors.content}
        />
      </ComponentWrapper>,
      ...customElements,
      <ComponentWrapper title="Are you finished?" key="submit">
        <div className="button-wrapper">
          <button
            className={classNames(
              'btn-send',
              { active: !isButtonDisabled },
              { error: this.state.error }
            )}
            onClick={isButtonDisabled ? this.checkForErrors : this.onSubmit}
          >
            {this.state.error && errors.button ? 'Please try again' : 'Submit'}
          </button>
        </div>
      </ComponentWrapper>
    ];
    return (
      <section className="post-form">
        {this.state.success !== '' && (
          <Success
            repeat={this.resetForm}
            mainPage={() => {
              this.props.history.push('/');
            }}
          />
        )}
        <Helmet>
          <title>Add new post</title>
        </Helmet>
        <div className="section-title">
          <h1>Add new post to your blog</h1>
        </div>
        <ul className="form-elements">{elements}</ul>
        <div className="post-form__custom-elements">
          <div className="element">
            <div className="clickable" onClick={this.addNewImage} />
            <a href="#" className="fa fa-camera-retro" />
          </div>
          <div className="element">
            <div className="clickable" onClick={this.addNewParagraph} />
            <a href="#" className="fa fa-pencil-square-o" />
          </div>
        </div>
      </section>
    );
  }
}

AddPost.propTypes = {
  toggleLoader: PropTypes.func,
  history: PropTypes.object,
  postActions: PropTypes.object
};

function mapDispatchToProps(dispatch) {
  return {
    postActions: bindActionCreators(postActions, dispatch)
  };
}

export default {
  component: withRouter(
    connect(
      null,
      mapDispatchToProps
    )(requireAdmin(AddPost))
  )
};
