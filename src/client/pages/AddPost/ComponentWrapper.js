import React from 'react';
import PropTypes from 'prop-types';

const ComponentWrapper = props => {
  return (
    <li className="component-wrapper">
      {props.onDelete && (
        <div className="wrapper-close">
          <a href="#" className="fa fa-times-circle" onClick={props.onDelete} />
        </div>
      )}
      <h3>{props.title}</h3>
      {props.children}
    </li>
  );
};

ComponentWrapper.propTypes = {
  onDelete: PropTypes.func,
  title: PropTypes.string,
  children: PropTypes.node
  // auth: PropTypes.object,
  // navOpen: PropTypes.bool
};

export default ComponentWrapper;
