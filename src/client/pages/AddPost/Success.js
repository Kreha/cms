import React, { Component } from 'react';
import PropTypes from 'prop-types';

import fadeTransition from './../../components/hocs/fadeTransition';

class Success extends Component {
  render() {
    return (
      <div className="success-screen">
        <h3 className="title">Great! Now where do you want to go?</h3>
        <div className="button-wrapper">
          <button className="btn-send active" onClick={this.props.repeat}>
            Add another post
          </button>
        </div>
        <div className="button-wrapper">
          <button className="btn-send active" onClick={this.props.mainPage}>
            Go to main page
          </button>
        </div>
      </div>
    );
  }
}

Success.propTypes = {
  repeat: PropTypes.func,
  newPost: PropTypes.func,
  mainPage: PropTypes.func
};

export default fadeTransition(Success);
