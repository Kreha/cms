import React from 'react';
import PropTypes from 'prop-types';
import fadeTransition from '../../components/hocs/fadeTransition';
import Loader from '../../components/Loader';

const PostListElement = props => {
  if (props.post.loader) {
    return (
      <div className="posts-list__list__list-element">
        <Loader small={true} />
      </div>
    );
  }
  let subtitle = null;
  if (props.post.subTitle) {
    if (props.post.subTitle.length > 30) {
      subtitle = `${props.post.subTitle.substring(0, 30)}...`;
    } else {
      subtitle = props.post.subTitle;
    }
  }
  return (
    <div
      onClick={props.getPost}
      className="posts-list__list__list-element loaded"
    >
      <h4 className="posts-list__list__list-element__title">
        {props.post.title}
      </h4>
      <img
        className="posts-list__list__list-element__img"
        src={props.post.img}
      />
      <div className="sub-data">
        <p className="posts-list__list__list-element__subTitle">{subtitle}</p>
        <p>Posted: {props.post.date}</p>
      </div>
    </div>
  );
};

PostListElement.propTypes = {
  postActions: PropTypes.object,
  post: PropTypes.object,
  getPost: PropTypes.func
};

export default fadeTransition(PostListElement);
