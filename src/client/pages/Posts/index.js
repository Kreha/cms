import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import * as postActions from './../../actions/postActions';
import PostListElement from './PostListElement';
import Post from './Post';

class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getPosts = () => {
    const postsToGet = Math.min(
      9,
      this.props.post.count - this.props.post.posts.length
    );
    this.props.postActions.createDummyPosts(postsToGet);
    this.props.postActions.getPosts(this.props.post.posts.length);
  };

  getPost = id => {
    this.props.postActions.getPost(id);
  };

  render() {
    const postData = this.props.post;
    const addMoreButton = postData.posts.length < this.props.post.count;
    let posts = [];
    if (postData.posts.length > 0) {
      posts = postData.posts.map(el => (
        <PostListElement
          post={el}
          key={el._id}
          getPost={() => {
            this.props.toggleLoader();
            this.getPost(el._id);
            this.props.disableLoader();
          }}
        />
      ));
    }
    return (
      <section className="post-list">
        <Helmet>
          <title>Check out my posts</title>
        </Helmet>
        {postData.selectedPost && (
          <Post
            post={postData.selectedPost}
            auth={this.props.auth}
            close={this.props.postActions.resetPost}
            addComment={this.props.postActions.addComment}
          />
        )}
        <div className="section-title">
          <h1>Check out my posts ;)</h1>
        </div>
        <ul className="posts-list__list">
          {posts}
          {addMoreButton && (
            <div className="button-wrapper normal">
              <button className="btn-send active" onClick={this.getPosts}>
                Get more posts
              </button>
            </div>
          )}
        </ul>
      </section>
    );
  }

  componentDidUpdate() {
    if (
      this.props.post.posts.length === 0 &&
      this.props.post.count > this.props.post.posts.length
    ) {
      const postsToGet = Math.min(
        9,
        this.props.post.count - this.props.post.posts.length
      );
      this.props.postActions.createDummyPosts(postsToGet);
      this.props.postActions.getPosts(this.props.post.posts.length);
    }
  }

  componentDidMount() {
    this.props.postActions.getPostsCount();
  }
}

Posts.propTypes = {
  postActions: PropTypes.object,
  post: PropTypes.object,
  auth: PropTypes.object,
  toggleLoader: PropTypes.func,
  disableLoader: PropTypes.func
};

function mapStateToProps(state) {
  return { post: state.post, auth: state.auth };
}

function mapDispatchToProps(dispatch) {
  return {
    postActions: bindActionCreators(postActions, dispatch)
  };
}

export default {
  component: withRouter(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(Posts)
  ),
  loadData: ({ dispatch }) => dispatch(postActions.getPostsCount())
};
