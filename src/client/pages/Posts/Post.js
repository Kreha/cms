import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import fadeTransition from '../../components/hocs/fadeTransition';
import Input from './../../components/Input';

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasOveflow: false,
      comment: '',
      error: ''
    };
  }

  onSubmit = () => {
    const today = new Date();
    const date =
      today.getFullYear() +
      '-' +
      (today.getMonth() + 1) +
      '-' +
      today.getDate();
    const minutes = today.getMinutes();
    const seconds = today.getSeconds();
    const time =
      today.getHours() +
      ':' +
      `${minutes.length === 1 ? '0' : ''}${minutes}` +
      ':' +
      `${seconds.length === 1 ? '0' : ''}${minutes}`;
    const dateTime = date + ' ' + time;
    const commentData = {
      comment: {
        date: dateTime,
        content: this.state.comment,
        author: this.props.auth.authenticated.user
      },
      postId: this.props.post._id
    };
    this.props.addComment(commentData);
    this.setState({ comment: '', error: '' });
  };

  onCommentChange = e => {
    this.setState({ comment: e.target.value });
  };
  render() {
    const isAuthenticated =
      this.props.auth.authenticated &&
      this.props.auth.authenticated.user &&
      this.props.auth.authenticated.token;
    const addComment = isAuthenticated ? (
      <div className="add-comment">
        <Input
          class="content-input"
          subClass="input--content textarea"
          type="textarea"
          placeholder="Add new comment"
          onChange={this.onCommentChange}
          value={this.state.comment}
          error={this.state.error}
        />
        <div className="button-wrapper">
          <button
            className={classNames(
              'btn-send',
              { active: this.state.comment !== '' },
              { error: this.state.error }
            )}
            onClick={this.onSubmit}
          >
            {this.state.error ? 'Please try again' : 'Add new comment'}
          </button>
        </div>
      </div>
    ) : (
      <p className="info">You need to log in if you want to add new comment</p>
    );
    let comments = null;
    if (this.props.post.comments && this.props.post.comments.length > 0) {
      comments = this.props.post.comments.map(el => {
        return (
          <li className="comment" key={el._id}>
            <h6 className="comment-author">
              <a className="fa fa-user-circle-o" />
              {el.author}
            </h6>
            <p className="comment-content">{el.content}</p>
            <p className="comment-date">{el.date}</p>
          </li>
        );
      });
    }
    let customs = null;

    if (this.props.post.customs && this.props.post.customs.length > 0) {
      customs = this.props.post.customs.map((el, i) => {
        if (el.elType === 'p') {
          return (
            <li className="p" key={i}>
              {el.title && <h6>{el.title}</h6>}
              <p>{el.content}</p>
            </li>
          );
        } else {
          return (
            <li className="img" key={i}>
              <img src={el.image} />
              {el.content && <h6>{el.content}</h6>}
            </li>
          );
        }
      });
    }
    return (
      <div className="posts-list__main-post">
        {this.state.hasOveflow && (
          <a className="fa fa-get-pocket scroll-sign scroll-up" />
        )}
        {this.state.hasOveflow && (
          <a className="fa fa-get-pocket scroll-sign scroll-down" />
        )}
        <div className="background" onClick={this.props.close} />
        <div id="post-content" className="content">
          <div className="close-button" onClick={this.props.close}>
            <a
              href="#"
              className="fa fa-times-circle"
              onClick={this.props.close}
            />
          </div>
          <h4>{this.props.post.title}</h4>
          <h5>{this.props.post.subTitle}</h5>
          <img src={this.props.post.img} />
          <p className="paragraph">{this.props.post.content}</p>
          <ul className="customs">{customs}</ul>
          {addComment}
          <ul className="comments">{comments}</ul>
        </div>
      </div>
    );
  }

  componentDidMount() {
    const content = document.getElementById('post-content');
    if (content.scrollHeight > content.offsetHeight) {
      this.setState({ hasOveflow: true });
    }
  }
}

Post.propTypes = {
  postActions: PropTypes.object,
  post: PropTypes.object,
  auth: PropTypes.object,
  close: PropTypes.func,
  addComment: PropTypes.func
};

export default fadeTransition(Post);
