import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import axios from 'axios';

import Input from './../components/Input';

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: {
        subject: '',
        message: '',
        contact: '',
        email: ''
      },
      errors: {
        subject: '',
        message: '',
        contact: '',
        email: ''
      },
      error: false
    };
  }

  handleChange = propertyName => e => {
    const { inputs } = this.state;
    const { errors } = this.state;

    inputs[propertyName] = e.target.value;
    errors[propertyName] = '';

    this.setState({ inputs, errors, error: false });
  };

  onSubmit = () => {
    if (!this.state.error) {
      this.setState({ error: false });
    }
    const { inputs } = this.state;
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const isEmailValid = re.test(String(inputs.contact).toLowerCase());
    if (isEmailValid) {
      this.props.toggleLoader();
      axios.post('/api/contact', this.state.inputs).then(response => {
        if (response.statusText !== 'OK') {
          this.setState({
            errors: { ...this.state.errors, message: response.data.error },
            error: true
          });
        }
        this.props.toggleLoader();
        this.setState({
          inputs: {
            subject: '',
            message: '',
            contact: ''
          }
        });
      });
    } else {
      if (!this.state.error) {
        this.setState({
          errors: {
            message: '',
            subject: '',
            contact: 'Are you sure this email is valid?'
          },
          error: true
        });
      }
    }
  };

  checkForErrors = () => {
    const { inputs } = this.state;
    const error = !inputs.subject || !inputs.contact || !inputs.message;
    let emailError = "Don't you want me to text you back? ;p";
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (inputs.contact) {
      const isEmailValid = re.test(String(inputs.contact).toLowerCase());
      emailError = isEmailValid ? '' : 'Are you sure this email is valid?';
    }
    const errors = {
      subject: inputs.title ? '' : 'Please add subject to your message',
      message: inputs.message ? '' : 'That should not be empty ;p',
      contact: inputs.contact ? '' : emailError
    };
    this.setState({ error, errors });
  };

  onNewsSubmit = () => {
    const { inputs } = this.state;
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const isEmailValid = re.test(String(inputs.email).toLowerCase());
    if (!isEmailValid) {
      this.setState({
        errors: {
          ...this.state.errors,
          email: 'Are you sure this email is valid?'
        }
      });
    } else {
      this.props.toggleLoader();
      axios
        .post('/api/newsletter/add', { email: inputs.email })
        .then(response => {
          if (response.data.error) {
            this.setState({
              errors: { ...this.state.errors, email: response.data.error },
              error: true
            });
          }
          this.props.toggleLoader();
          this.setState({
            inputs: {
              subject: '',
              message: '',
              contact: '',
              email: ''
            }
          });
        });
    }
  };

  render() {
    const { inputs, errors } = this.state;
    const isButtonDisabled =
      !inputs.subject || !inputs.contact || !inputs.message;
    return (
      <section className="contact-page">
        <Helmet>
          <title>Contact me</title>
        </Helmet>
        <div className="section-title">
          <h1>Do you want to tell me something?</h1>
        </div>
        <div className="left-side">
          <Input
            class="title-input"
            subClass="input--title"
            type="text"
            onChange={this.handleChange('subject')}
            placeholder="Type in subject for your message"
            value={inputs.subject}
            error={errors.subject}
          />
          <Input
            class="title-input"
            subClass="input--title"
            type="text"
            onChange={this.handleChange('contact')}
            placeholder="How can I contact you? Type your email"
            value={inputs.contact}
            error={errors.contact}
          />
          <Input
            class="content-input"
            subClass="input--content textarea"
            type="textarea"
            placeholder="Your message"
            onChange={this.handleChange('message')}
            value={inputs.message}
            error={errors.message}
          />
          <div className="button-wrapper">
            <button
              className={classNames(
                'btn-send',
                { active: !isButtonDisabled },
                { error: this.state.error }
              )}
              onClick={isButtonDisabled ? this.checkForErrors : this.onSubmit}
            >
              Submit
            </button>
          </div>
          <div className="newsletter">
            <div className="section-title">
              <h1>Maybe you want to receive newsletter?</h1>
            </div>
            <Input
              class="title-input"
              subClass="input--title"
              type="text"
              onChange={this.handleChange('email')}
              placeholder="Type in your email"
              value={inputs.email}
              error={errors.email}
            />
            <div className="button-wrapper">
              <button
                className={classNames(
                  'btn-send',
                  { active: inputs.email !== '' },
                  { error: errors.email !== '' }
                )}
                onClick={inputs.email === '' ? null : this.onNewsSubmit}
              >
                Submit
              </button>
            </div>
          </div>
        </div>
        <div className="right-side">
          <h3>
            Hi ;) I&apos;m glad you want to contact me. You can do that using
            this form. However if you prefer sending me email directly from your
            address you can send it to cmscontact1234@gmail.com. Cheers ;)
          </h3>
        </div>
      </section>
    );
  }
}

Contact.propTypes = {
  toggleLoader: PropTypes.func
};

export default {
  component: connect(
    null,
    null
  )(Contact)
};
