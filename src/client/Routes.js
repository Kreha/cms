import App from './App';
import Posts from './pages/Posts';
import Contact from './pages/Contact';
import AddPost from './pages/AddPost';

export default [
  {
    ...App,
    routes: [
      {
        ...Posts,
        exact: true,
        path: '/'
      },
      {
        ...AddPost,
        exact: true,
        path: '/add'
      },
      {
        ...Contact,
        exact: true,
        path: '/contact'
      }
    ]
  }
];
