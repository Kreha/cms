import {
  AUTH_USER,
  TOGGLE_MODAL,
  ERROR_LOGIN,
  ERROR_PASSWORD,
  RESET_ERRORS
} from './../constants/authActionTypes';

const INITIAL_STATE = {
  authenticated: null,
  modalOpen: false,
  errorLogin: null,
  errorPassword: null
};

export function authReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case AUTH_USER: {
      return {
        ...state,
        authenticated: action.payload,
        modalOpen: false,
        errorLogin: null,
        errorPassword: null
      };
    }
    case TOGGLE_MODAL: {
      return {
        ...state,
        modalOpen: !state.modalOpen,
        errorLogin: null,
        errorPassword: null
      };
    }
    case ERROR_LOGIN: {
      return {
        ...state,
        errorLogin: action.payload
      };
    }
    case ERROR_PASSWORD: {
      return {
        ...state,
        errorPassword: action.payload
      };
    }
    case RESET_ERRORS: {
      return {
        ...state,
        errorPassword: null,
        errorLogin: null
      };
    }

    default: {
      return state;
    }
  }
}
