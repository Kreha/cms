import { combineReducers } from 'redux';
import { authReducer } from './auth';
import { post } from './post';

const rootReducer = combineReducers({
  auth: authReducer,
  post
});

export default rootReducer;
