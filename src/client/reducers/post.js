import {
  GET_DUMMY_POSTS,
  GET_POST_COUNT,
  REPLACE_DUMMY_POSTS,
  RESET_POSTS,
  GET_MAIN_POST,
  RESET_POST,
  ADD_COMMENT
} from './../constants/postActionTypes';

const INITIAL_STATE = {
  posts: [],
  selectedPost: null,
  count: 0
};

export function post(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_DUMMY_POSTS: {
      const dummyPosts = [];
      for (let i = 0; i < action.payload.length; i++) {
        dummyPosts.push({ _id: i, loader: true, dummy: true });
      }
      return {
        ...state,
        posts: [...state.posts, ...dummyPosts]
      };
    }
    case GET_POST_COUNT: {
      return {
        ...state,
        count: action.payload.count
      };
    }
    case RESET_POST: {
      return {
        ...state,
        selectedPost: null
      };
    }
    case ADD_COMMENT: {
      return {
        ...state,
        selectedPost: {
          ...state.selectedPost,
          comments: [action.payload.comment, ...state.selectedPost.comments]
        }
      };
    }
    case GET_MAIN_POST: {
      return {
        ...state,
        selectedPost: action.payload.post
      };
    }
    case RESET_POSTS: {
      return {
        ...state,
        posts: [],
        selectedPost: null
      };
    }
    case REPLACE_DUMMY_POSTS: {
      const indexToDeleteFrom = state.posts.findIndex(el => el.dummy);
      return {
        ...state,
        posts: [
          ...state.posts.slice(0, indexToDeleteFrom),
          ...action.payload.posts
        ]
      };
    }
    default: {
      return state;
    }
  }
}
