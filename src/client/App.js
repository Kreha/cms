import React, { Component } from 'react';
import { renderRoutes } from 'react-router-config';
import Navbar from './components/Navbar';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import * as authActions from './actions/authActions';
import * as postActions from './actions/postActions';
import SignModal from './components/modals/SignModal';
import Loader from './components/Loader';
import './styles/index.scss';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { navOpen: false, loader: false };
  }

  toggleNav = () => {
    this.setState({
      navOpen: !this.state.navOpen
    });
  };
  toggleLoader = () => {
    this.setState({
      loader: !this.state.loader
    });
  };

  disableLoader = () => {
    this.setState({
      loader: false
    });
  };

  render() {
    return (
      <main className="main">
        <h1 className="main-title">SomeFancyTitleForMyBlog ;)</h1>
        {this.state.loader && <Loader small={false} />}
        <SignModal open={this.props.auth.modalOpen} />
        <Navbar
          toggleNav={this.toggleNav}
          navOpen={this.state.navOpen}
          toggleSignModal={this.props.authActions.toggleModal}
        />
        <section className="content">
          {renderRoutes(this.props.route.routes, {
            toggleLoader: this.toggleLoader,
            disableLoader: this.disableLoader
          })}
        </section>
      </main>
    );
  }

  componentDidMount() {
    this.props.authActions.getUser();
    this.props.postActions.getPostsCount();
  }
}

App.propTypes = {
  route: PropTypes.object,
  authActions: PropTypes.object,
  postActions: PropTypes.object,
  auth: PropTypes.object
};

function mapStateToProps(state) {
  return { auth: state.auth };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    postActions: bindActionCreators(postActions, dispatch)
  };
}

export default {
  component: withRouter(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(App)
  ),
  loadData: ({ dispatch }) => dispatch(authActions.getUser())
};
