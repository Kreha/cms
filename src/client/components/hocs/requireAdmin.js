import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

export default ChildComponent => {
  class RequireAdmin extends Component {
    render() {
      const isSignedIn =
        !!this.props.auth.authenticated && !!this.props.auth.authenticated.user;
      const isAdmin = isSignedIn && this.props.auth.authenticated.isAdmin;
      switch (isAdmin) {
        case true:
          return <ChildComponent {...this.props} />;
        default:
          return <Redirect to={'/'} />;
      }
    }
  }

  function mapStateToProps(state) {
    return { auth: state.auth };
  }

  return connect(mapStateToProps)(RequireAdmin);
};
