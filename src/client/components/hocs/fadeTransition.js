import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

const fadeTransition = WrappedComponent => {
  const TransitionedComponent = props => (
    <ReactCSSTransitionGroup
      transitionAppear={true}
      transitionLeave={true}
      transitionAppearTimeout={600}
      transitionEnterTimeout={600}
      transitionLeaveTimeout={400}
      transitionName="fade"
    >
      <WrappedComponent {...props} />
    </ReactCSSTransitionGroup>
  );
  return TransitionedComponent;
};

export default fadeTransition;
