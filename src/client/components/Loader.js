import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import fadeTransition from './hocs/fadeTransition';

class LoaderComponent extends Component {
  render() {
    return (
      <div
        className={classNames('loader-wrapper', {
          small: this.props.small
        })}
      >
        {!this.props.small && (
          <h3 className="loader-text">Give us a moment....</h3>
        )}
        <div
          className={classNames('loader loader--circularSquare', {
            small: this.props.small
          })}
        />
      </div>
    );
  }
}

LoaderComponent.propTypes = {
  small: PropTypes.bool
};

export default fadeTransition(LoaderComponent);
