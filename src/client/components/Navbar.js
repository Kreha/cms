import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import * as authActions from './../actions/authActions';
import * as postActions from './../actions/postActions';

class Navbar extends Component {
  openModalAndCloseNav = isSignedIn => {
    if (isSignedIn) {
      this.props.authActions.signOut();
    } else {
      this.props.toggleNav();
      this.props.authActions.toggleModal();
    }
  };
  render() {
    const isSignedIn =
      !!this.props.auth.authenticated && !!this.props.auth.authenticated.user;
    const isAdmin = isSignedIn && this.props.auth.authenticated.isAdmin;
    const isMainSite = this.props.location.pathname === '/';
    return (
      <div
        id="circularMenu1"
        className={classNames({
          'circular-menu circular-menu-left': true,
          active: this.props.navOpen,
          admin: isAdmin
        })}
      >
        <a
          className="floating-btn"
          onClick={() => {
            this.props.toggleNav();
            if (!isMainSite) {
              this.props.postActions.resetPosts();
            }
          }}
        >
          <i className="fa fa-bars" />
        </a>

        <menu className="items-wrapper">
          <Link
            to="/"
            className="menu-item fa fa-home"
            onClick={() => {
              this.props.toggleNav();
              if (!isMainSite) {
                this.props.postActions.resetPosts();
              }
            }}
          />
          <a
            href="#"
            className={classNames({
              'menu-item fa': true,
              'fa-sign-in': !isSignedIn,
              'fa-sign-out': isSignedIn
            })}
            onClick={() => this.openModalAndCloseNav(isSignedIn)}
          />
          <Link
            to="/contact"
            className="menu-item fa fa-envelope-o"
            onClick={() => {
              this.props.toggleNav();
              if (!isMainSite) {
                this.props.postActions.resetPosts();
              }
            }}
          />
          {isAdmin && (
            <Link
              to="/add"
              className="menu-item fa fa-plus-circle"
              onClick={() => {
                this.props.toggleNav();
                if (!isMainSite) {
                  this.props.postActions.resetPosts();
                }
              }}
            />
          )}
        </menu>
      </div>
    );
  }
}

Navbar.propTypes = {
  toggleNav: PropTypes.func,
  authActions: PropTypes.object,
  postActions: PropTypes.object,
  auth: PropTypes.object,
  location: PropTypes.object,
  navOpen: PropTypes.bool
};

function mapStateToProps(state) {
  return { auth: state.auth };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    postActions: bindActionCreators(postActions, dispatch)
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Navbar)
);
