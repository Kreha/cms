import React from 'react';
import Dropzone from 'react-dropzone';
import classNames from 'classnames';

class DropzoneComponent extends React.Component {
  constructor() {
    super();
    this.state = { imagePreview: null };
  }

  onDrop = acceptedFiles => {
    const reader = new FileReader();
    reader.onload = e => {
      this.setState({
        imagePreview: e.target.result
      });
    };
    reader.readAsDataURL(acceptedFiles[0]);
    this.props.onDrop(acceptedFiles[0]);
  };

  render() {
    return (
      <div className={`post-form dropzone dropzone${this.props.class}`}>
        <Dropzone onDrop={this.onDrop} accept="image/*">
          {({ getRootProps, getInputProps, isDragActive }) => {
            return (
              <div
                {...getRootProps()}
                className={classNames('dropzone', {
                  'dropzone--isActive': isDragActive,
                  'transparent-dropzone':
                    this.state.imagePreview && this.props.isImage,
                  error: this.props.error
                })}
              >
                <input {...getInputProps()} />
                {this.props.error ? (
                  <p>Please choose some image</p>
                ) : isDragActive ? (
                  <p>Drop file here...</p>
                ) : (
                  <p>
                    Do you need title picture? Drop it here or simply click and
                    select it
                  </p>
                )}
              </div>
            );
          }}
        </Dropzone>
        <img
          id="target"
          src={this.props.isImage ? this.state.imagePreview : null}
        />
        {this.props.changeCaption && (
          <div className="input-holder img-caption">
            <input
              className="input input--caption"
              type="text"
              placeholder=" "
              onChange={this.props.changeCaption}
              value={this.props.caption}
              ref={input => {
                this.titleInput = input;
              }}
            />
            <div
              className="placeholder placeholder--caption"
              onClick={() => {
                this.titleInput.focus();
              }}
            >
              Maybe you want to add some caption for the image?
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default DropzoneComponent;
