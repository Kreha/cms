import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from './../../actions/authActions';

class SignModal extends React.Component {
  state = {
    inputs: {
      email: '',
      password: '',
      passwordCheck: ''
    },
    modalTypeIn: true,
    showPassword: false
  };

  handleChange = propertyName => e => {
    const { inputs } = this.state;

    inputs[propertyName] = e.target.value;
    this.props.authActions.resetErrors();
    this.setState({ inputs });
  };

  changeModalType = () => {
    this.setState({ modalTypeIn: !this.state.modalTypeIn });
  };

  toggleShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };

  submitForm = () => {
    const { email, password, passwordCheck } = this.state.inputs;
    if (this.state.modalTypeIn) {
      this.props.authActions.signIn({
        email,
        password
      });
    } else {
      this.props.authActions.signUp(
        {
          email,
          password,
          passwordCheck
        }
        // , () => {
        //   this.props.history.push('/tajnasciezka'); moge tego uzyc w akcji i od razu przeniesc ziomka gdzies
        // }
      );
    }
  };

  render() {
    const isSignInModal = this.state.modalTypeIn;
    return (
      <div>
        <Dialog
          open={this.props.open}
          onClose={this.props.authActions.toggleModal}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">
            {isSignInModal
              ? 'Sign in to existing account'
              : 'Create new account'}
          </DialogTitle>
          <DialogContent>
            {/* <DialogContentText>
                {isSignInModal ?
                  'Sign in to my blog' :
                  'Create new account'
              }
              </DialogContentText> */}
            <TextField
              error={!!this.props.auth.errorLogin}
              autoFocus
              margin="dense"
              id="email"
              label={
                this.props.auth.errorLogin
                  ? this.props.auth.errorLogin
                  : 'Email Address'
              }
              type="email"
              fullWidth
              value={this.state.inputs.email}
              onChange={this.handleChange('email')}
            />
            <TextField
              error={!!this.props.auth.errorPassword}
              margin="dense"
              fullWidth
              type={this.state.showPassword ? 'text' : 'password'}
              id="pass"
              label={
                this.props.auth.errorPassword
                  ? this.props.auth.errorPassword
                  : 'Password'
              }
              value={this.state.inputs.password}
              onChange={this.handleChange('password')}
              InputProps={{
                endAdornment: (
                  <InputAdornment variant="filled" position="end">
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={this.toggleShowPassword}
                    >
                      {this.state.showPassword ? (
                        <VisibilityOff />
                      ) : (
                        <Visibility />
                      )}
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
            {!isSignInModal && (
              <TextField
                margin="dense"
                fullWidth
                type={this.state.showPassword ? 'text' : 'password'}
                id="passCheck"
                label="Retype your password"
                value={this.state.inputs.passwordCheck}
                onChange={this.handleChange('passwordCheck')}
                InputProps={{
                  endAdornment: (
                    <InputAdornment variant="filled" position="end">
                      <IconButton
                        aria-label="Toggle password visibility"
                        onClick={this.toggleShowPassword}
                      >
                        {this.state.showPassword ? (
                          <VisibilityOff />
                        ) : (
                          <Visibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  )
                }}
              />
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.changeModalType} color="primary">
              {isSignInModal
                ? 'I need new account'
                : 'I already have an account'}
            </Button>
            <Button
              onClick={this.props.authActions.toggleModal}
              color="primary"
            >
              Cancel
            </Button>
            <Button onClick={this.submitForm} color="primary">
              {isSignInModal ? 'Sign in' : 'Create new account'}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

SignModal.propTypes = {
  open: PropTypes.bool,
  authActions: PropTypes.object,
  modalType: PropTypes.string,
  auth: PropTypes.object
};

function mapStateToProps(state) {
  return { auth: state.auth };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignModal);
