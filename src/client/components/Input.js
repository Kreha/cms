import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class Input extends Component {
  render() {
    const inputElement =
      this.props.type === 'text' ? (
        <input
          className={`input ${this.props.subClass}`}
          type="text"
          placeholder=" "
          onChange={this.props.onChange}
          value={this.props.value}
          ref={input => {
            this.input = input;
          }}
        />
      ) : (
        <textarea
          className={classNames(`input ${this.props.subClass}`, {
            filled: this.props.value !== ''
          })}
          onChange={this.props.onChange}
          value={this.props.value}
          ref={input => {
            this.input = input;
          }}
        />
      );

    return (
      <div className={`input-holder ${this.props.class}`}>
        {inputElement}
        <div
          className="placeholder"
          onClick={() => {
            this.input.focus();
          }}
        >
          {this.props.placeholder}
        </div>
        {this.props.error !== '' && (
          <div className="error">{this.props.error}</div>
        )}
      </div>
    );
  }
}

Input.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string,
  subClass: PropTypes.string,
  class: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  error: PropTypes.string
};

export default Input;
