import jwt from 'jwt-simple';
import { secretString } from '../../config';
import User from '../models/user';

const tokenForUser = user => {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, secretString);
};

const signin = function(req, res) {
  //user had his em and pass auth'd so now his going to receive token
  //passport assigns user (inside comparePassword) to req.user
  if (req.user.error) {
    res.send(req.user);
  } else {
    const token = tokenForUser(req.user);
    req.session.user = {
      user: req.user.email,
      isAdmin: req.user.isAdmin,
      token,
      id: req.user._id
    };
    req.session.save(function(err) {
      if (err) {
        throw err;
      }
      res.json(req.session.user);
    });
  }
};

const signup = (req, res, next) => {
  const { email, password, passwordCheck } = req.body;
  if (!email || !password || !passwordCheck) {
    return res.send({ errorLogin: 'Provide email and both passwords' });
  }
  if (passwordCheck !== password) {
    return res.send({ errorPassword: 'Both passwords must be the same' });
  }
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const isEmailValid = re.test(String(email).toLowerCase());
  if (!isEmailValid) {
    return res.send({ errorLogin: "This email doesn't look right.... " });
  }

  User.findOne({ email }, (err, existingUser) => {
    if (err) {
      return next(err);
    }

    if (existingUser) {
      return res.status(422).send({ errorLogin: 'Email is already in use' });
    }

    const user = new User({ email, password });
    user.save(err => {
      if (err) {
        return next(err);
      }
      const token = tokenForUser(user);
      req.session.user = {
        user: user.email,
        isAdmin: false,
        token
      };
      req.session.save(function(err) {
        if (err) {
          throw err;
        }
        res.json(req.session.user);
      });
    });
  });
};

export { signin, signup };
