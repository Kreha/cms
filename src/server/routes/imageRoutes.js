import express from 'express';
import AWS from 'aws-sdk';
import uuid from 'uuid/v1';

import { accessKeyId, secretAccessKey } from './../../config';

const s3 = new AWS.S3({
  signatureVersion: 'v4',
  accessKeyId,
  secretAccessKey,
  region: 'eu-central-1'
});

const imageRouter = express.Router();

imageRouter.post('/upload', (req, res) => {
  const type = req.body.ContentType;
  const Key = `${req.session.user.id}/${uuid()}.${type}`;

  s3.getSignedUrl(
    'putObject',
    {
      Bucket: 'cms-bucket-06',
      ContentType: type,
      Key
    },
    (err, url) => res.send({ Key, url })
  );
});

export default imageRouter;
