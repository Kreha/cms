import express from 'express';

import Post from './../models/post';
import Comment from './../models/comment';

const postRouter = express.Router();

postRouter.post('/upload', (req, res) => {
  const post = req.body;
  const newPost = new Post(post);
  newPost.save(err => {
    if (err) {
      return res.send({ error: 'Something went wrong' });
    }
    res.json(newPost);
  });
});

postRouter.post('/add-comment', (req, res) => {
  const { comment, postId } = req.body;
  const newComment = new Comment(comment);
  newComment.save(err => {
    if (err) {
      return res.send({ error: 'Something went wrong' });
    }
    Post.update(
      { _id: postId },
      {
        $push: {
          comments: newComment._id
        }
      },
      function(err) {
        if (err) {
          return res.send(err);
        }
        return res.json(newComment);
      }
    );
  });
});

postRouter.post('/count', (req, res) => {
  Post.count({}, function(err, count) {
    res.send({ count });
  });
});

postRouter.get('/get/:skip/:limit', (req, res) => {
  const { skip, limit } = req.params;
  const query = Post.find()
    .skip(parseInt(skip))
    .limit(parseInt(limit))
    .sort('-date')
    .select({ title: 1, img: 1, date: 1, likes: 1, subTitle: 1, dateValue: 1 });

  query.exec((err, docs) => {
    if (err) {
      return res.send({ error: 'Something went wrong' });
    }
    res.json(docs);
  });
});

postRouter.post('/get', (req, res) => {
  const _id = req.body._id;
  Post.findById(_id)
    .populate('comments')
    .exec(function(err, doc) {
      if (err) {
        return res.send({ error: 'Something went wrong' });
      }
      res.json(doc);
    });
});

export default postRouter;
