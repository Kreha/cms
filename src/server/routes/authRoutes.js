import express from 'express';
import { signup, signin } from '../controllers/authentication';
import { jwtLogin, localLogin } from '../services/passport';
import passport from 'passport';

passport.use(jwtLogin);
passport.use(localLogin);
//if user is auth'd do not create session for him
// const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

const authRouter = express.Router();
authRouter.post('/session/get', (req, res) => {
  if (typeof req.session.user !== 'undefined' && req.session.user.token) {
    res.json(req.session.user);
  } else {
    res.json({ msg: 'no signed user' });
  }
});
authRouter.post('/session/delete', (req, res) => {
  req.session.user = {};
  req.session.save(function(err) {
    if (err) {
      throw err;
    }
    res.json(req.session.user);
  });
});
// authRouter.get('/', requireAuth, function(req, res) {
//   // res.send({ message: 'Super secret code is ABC123' });
//   // if(typeof req.session.user !== 'undefined') {
//   //   res.json(req.session.user);
//   // }
// });
authRouter.post('/signin', requireSignin, signin);
authRouter.post('/signup', signup);
export default authRouter;
