import express from 'express';
import nodemailer from 'nodemailer';
import Newsletter from './../models/newsletter';

import { mailLogin, mailPassword } from './../../config';

const newsletterRouter = express.Router();
const id = '5c4a2ebbf42acb0a643ff007';

newsletterRouter.post('/create', (req, res) => {
  const newsletter = new Newsletter({ emails: [] });
  newsletter.save(err => {
    if (err) {
      return res.send({ error: 'Something went wrong' });
    }
    return res.send({ ok: 'created' });
  });
});

newsletterRouter.post('/add', function(req, res) {
  const email = req.body.email;
  Newsletter.update(
    { _id: id },
    {
      $push: {
        emails: email
      }
    },
    function(err) {
      if (err) {
        return res.send(err);
      }
      return res.json({ ok: 'ok' });
    }
  );
});

newsletterRouter.post('/send', function(req, res) {
  const title = req.body.title;
  Newsletter.findById(id, function(err, list) {
    if (err) {
      return res.send({ error: 'Something went wrong' });
    }
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: mailLogin,
        pass: mailPassword
      },
      tls: {
        rejectUnauthorized: false
      }
    });
    list.emails.forEach(email => {
      const mailOptions = {
        from: 'PostBlog',
        to: email,
        subject: 'New post on my blog!',
        text: `Hi! I just added new post: ${title}. Come check it out!`
      };
      transporter.sendMail(mailOptions, function(err) {
        if (err)
          res.send({ error: 'Something went wrong...... please try again :(' });
      });
    });
  });
  return res.send({ ok: 'ok' });
});

export default newsletterRouter;
