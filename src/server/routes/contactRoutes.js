import express from 'express';
import nodemailer from 'nodemailer';

import { mailLogin, mailPassword } from './../../config';

const contactRouter = express.Router();

contactRouter.post('*', (req, res) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: mailLogin,
      pass: mailPassword
    },
    tls: {
      rejectUnauthorized: false
    }
  });

  const mailOptions = {
    from: 'mailLogin',
    to: 'cmscontact1234@gmail.com',
    subject: req.body.subject,
    text: req.body.message,
    replyTo: req.body.contact
  };

  transporter.sendMail(mailOptions, function(err, info) {
    if (err)
      res.send({ error: 'Something went wrong...... please try again :(' });
    else res.send(info);
  });
});

export default contactRouter;
