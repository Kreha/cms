import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../../client/reducers';
import axios from 'axios';

export default req => {
  const axiosInstance = axios.create({
    headers: { cookie: req.get('cookie') || '' }
  });
  const store = createStore(
    reducers,
    {
      auth: {
        authenticated: req.session ? req.session.user || null : null,
        modalOpen: false,
        errorLogin: null,
        errorPassword: null
      }
    },
    applyMiddleware(thunk.withExtraArgument(axiosInstance))
  );

  return store;
};
