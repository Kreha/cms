import connectMongo from 'connect-mongo';
import session from 'express-session';
import mongoose from 'mongoose';
const MongoStore = connectMongo(session);

const useSession = app => {
  app.use(
    session({
      secret: 'mySecretString',
      saveUninitialized: false,
      resave: false,
      cookie: { maxAge: 1000 * 60 * 60 * 24 * 2 },
      store: new MongoStore({
        mongooseConnection: mongoose.connection,
        ttl: 2 * 24 * 60 * 60
      })
    })
  );
};

export default useSession;
