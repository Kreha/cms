const requireLogin = (req, res, next) => {
  if (typeof req.session.user === 'undefined' || !req.session.user.token) {
    return res.send({ error: 'Please sign in' });
  }

  next();
};

export default requireLogin;
