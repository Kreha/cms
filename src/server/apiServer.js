import 'babel-polyfill';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import logger from 'morgan';
import mongoose from 'mongoose';
import imageRoutes from './routes/imageRoutes';
import postRoutes from './routes/postRoutes';
import contactRoutes from './routes/contactRoutes';
import newsletterRoutes from './routes/newsletterRoutes';
import useSession from './helpers/useSession';

const app = express();
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
mongoose.connect('mongodb://kreha:cms123@ds145562.mlab.com:45562/cms');

useSession(app);

app.use('/img', imageRoutes);
app.use('/post', postRoutes);
app.use('/contact', contactRoutes);
app.use('/newsletter', newsletterRoutes);

app.listen(3001, function(err) {
  if (err) {
    console.log(err);
  }
  console.log('api server on port 3001');
});
