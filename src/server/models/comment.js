import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const commentSchema = new Schema({
  date: String,
  content: String,
  author: String
});

const modelClass = mongoose.model('comment', commentSchema);

export default modelClass;
