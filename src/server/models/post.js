import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const postSchema = new Schema({
  date: String,
  title: String,
  subTitle: String,
  content: String,
  img: String,
  comments: [{ type: Schema.Types.ObjectId, ref: 'comment' }],
  likes: { type: Number, default: 0 },
  customs: [
    {
      elType: String,
      content: String,
      title: String,
      image: String
    }
  ],
  dateValue: String
});

const modelClass = mongoose.model('post', postSchema);

export default modelClass;
