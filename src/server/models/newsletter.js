import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const newsletterSchema = new Schema({
  emails: []
});

const modelClass = mongoose.model('newsletter', newsletterSchema);

export default modelClass;
