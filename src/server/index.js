import 'babel-polyfill';
import express from 'express';
import proxy from 'http-proxy';
import mongoose from 'mongoose';
import { matchRoutes } from 'react-router-config';
import bodyParser from 'body-parser';
import logger from 'morgan';
import renderer from './helpers/renderer';
import createStore from './helpers/createStore';
import Routes from './../client/Routes';
import authRoutes from './routes/authRoutes';
import useSession from './helpers/useSession';

mongoose.connect('mongodb://kreha:cms123@ds145562.mlab.com:45562/cms');
const app = express();
const apiProxy = proxy.createProxyServer({ target: 'http://localhost:3001' });

app.use('/api', function(req, res) {
  apiProxy.web(req, res);
});

app.use(express.static('public'));
app.use(logger('dev'));
app.use(bodyParser.json({ type: '*/*' }));
app.use(bodyParser.urlencoded({ extended: false }));

useSession(app);

app.get('*', (req, res) => {
  //req does contain cookies so I need it inside createStore
  const store = createStore(req);

  const promises = matchRoutes(Routes, req.path)
    .map(({ route }) => {
      return route.loadData ? route.loadData(store) : null;
    })
    .map(promise => {
      if (promise) {
        return new Promise((resolve, reject) => {
          //this new Promise wrapping original one is going to be resolved no matter what
          promise.then(resolve).catch(resolve);
        });
      }
    });

  //connecting all the promises into one
  Promise.all(promises).then(() => {
    const context = {};

    const content = renderer(req, store, context);

    if (context.url) {
      return res.redirect(301, context.url);
    }
    if (context.notFound) {
      res.status(404);
    }

    res.send(content);
  });
});

app.use('/auth', authRoutes);

app.listen(3000, function(err) {
  if (err) {
    console.log(err);
  }
  console.log('main server on port 3000');
});
